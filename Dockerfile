#
# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
#


# Copy
FROM maven:3.6-openjdk-17-slim AS build
WORKDIR /home/wrapper
COPY pom.xml .
COPY src/main/java ./src/main/java
COPY src/main/resources ./src/main/resources

# Build
RUN mvn -f pom.xml clean package

# Package
FROM openjdk:17-jdk-slim
COPY --from=build /home/wrapper/target/*.jar /usr/local/lib/wrapper.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/wrapper.jar"]