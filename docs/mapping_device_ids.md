# Mapping Devices and Devices Instances to IDS
Sample JSON API responses copied from API reference: https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/iotbroker/core/-/blob/2ff19be9fb29b84f23387174a70b042f27d8cc64/device-registry-service/openapi.yaml
(Accessed on May 12th, 2022)


## Device type to catalog

```json
[
  {
    "id": "652e9dd6-881d-11ec-a8a3-0242ac120002",
    "source": "lowcosttracker",
    "identifier": "lowcosttracker_v1",
    "providedBy": [
      "lowcosttracker-adapter"
    ],
    "description": "Devices for locating and tracking goods.",
    "enabled": true,
    "autoRegisterDeviceInstances": true,
    "autoEnableDeviceInstances": true
  }
]
```

The DSC Catalog has the fields `title` & `description`. The device type will be mapped as follows:


| DSC Catalog Property   | API Response Property |
|------------------------|-----------------------|
| `title`                | `identifier`          |
| `description`          | `description`         |



## Device instances to resources

```json
[
  {
    "id": "652e9dd6-881d-11ec-a8a3-0242ac120002",
    "source": "lowcosttracker",
    "tenant": "Tracker-42",
    "deviceTypeIdentifier": "lowcosttracker_v1",
    "registrationTime": "2020-11-12T12:56:55Z",
    "lastSeen": "2020-11-12T12:56:55Z",
    "enabled": true,
    "description": "Device for locating and tracking goods.",
    "hardwareRevision": "v1.0.0",
    "firmwareVersion": "v1.0.0"
  }
]
```

The DSC Resource has the fields `title`, `description`, `keywords`, `publisher`, `sovereign`, 
`language`, `license`, `endpointDocumentation`, and `paymentMethod`.
The device type will be mapped as follows:


| DSC Resource Property         | API Response Property |
|-------------------------------|-----------------------|
| `title` (String)              | `id`                  |
| `description` (String)        | `description`         |
| `keywords` (List of String)   | `source`, `tenant`    |
| `publisher` (URI)             | - (?)                 | # TODO
| `sovereign` (URI)             | - (?)                 | # TODO
| `language` (String)           | `en`                  |
| `license` (URI)               | - (?)                 | # TODO
| `endpointDocumentation` (URI) | - (?)                 | # TODO
| `paymentMethod` (Enum)        | `free`, `undefined`   |


