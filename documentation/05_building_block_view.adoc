[[section-building-block-view]]
== Building Block View

The building block view is a static and hierarchical decomposition of the system into building blocks and their relationships.
It is a collection of black box and white box descriptions for all important components.
Building blocks may be modules, components, subsystems, classes, interfaces, packages, libraries, frameworks, layers, partitions, tiers, functions, macros, operations, data structures, etc.

In blackbox view, only the _interface_ of the component is visible, not its internals.
However, for interface specifications including all method signatures and their descriptions we refer to the _JavaDoc_ available on request, or to the _OpenAPI_ documents.
Only other kinds of interfaces are described here.
This is a simplification of arc42 (which requires a short description of interfaces), to avoid redundancy and inconsistencies, and to reduce documentation efforts.

=== Overview

The following diagram shows an overview of how this project's custom components integrate into the IoT Broker.

[[iot-broker-integration-overview]]
.IDS Wrapper - IoT Broker integration overview
image::images/05_ids_wrapper_iot_broker_integration_overview.png[]

=== Building Blocks - Level 1 - Custom Components Whitebox

In level 1, this project's custom components and their relationships are shown.
This is a simplification of the <<iot-broker-integration-overview>>.

.IDS Wrapper - IoT Broker integration: Building Blocks - Level 1
image::images/05_ids_wrapper_building_block_level_1_overview.png[]

There are the following components with its responsibilities:

[cols="1,1,3",options="header"]
|===
|Name
|Status
|Responsibility

|IoT Broker IDS Wrapper
|custom development
|The wrapper encapsulates and simplifies the connection of the IoT Broker to the IDS (via the DataSpace Connector)

|IDS Connector
|DataSpace Connector
|External access / public interfaces only for IDS connections
|===

For each component a brief overview is given as a blackbox view.
Each one is implemented as a microservice.
For more details see <<Building Blocks - Level 2>> for the whitebox views of the components.

==== IDS Integration

===== IDS Connector (Blackbox)

The IDS Connector provides data to other companies and other SE platforms via the IDS (link:https://en.wikipedia.org/wiki/International_Data_Spaces[International Data Spaces]).
Its architecture conforms to standard link:https://www.aisec.fraunhofer.de/de/presse-und-veranstaltungen/presse/pressemitteilungen/2020/DINSPEC27070.html[DIN SPEC 27070:2020-03] and enables to enforce data sovereignty.
There are different implementations of this standard - the IoT Broker uses a DataSpace Connector (see link:https://www.dataspace-connector.io[] for further information).

NOTE: Since the IDS Connector is developed outside this project, there is no whitebox view of it in this document.

===== IoT Broker IDS Wrapper (Blackbox)

The IoT Broker IDS Wrapper facilitates the communication of the IoT Broker services with the IDS Connector.
The wrapper manages the process of creating the necessary IDS resources at the Connector in addition to handling the requests coming through the Connector.
The Wrapper receives the requests from the IDS Connector, parses them, then forwards them to the respective IoT Broker service APIs, such as the Sensor Data History Service and the Device Registry Service APIs.
The IDS Wrapper achieves this by communicating with the management interface of the IDS Connector and performing the necessary requests internally to create and manage the IDS resource.
To forward the requests, the Wrapper contains two controllers that handle the incoming requests from the Connector and forwards them to the corresponding endpoint in the IoT Broker services.

=== Building Blocks - Level 2

In level 2, the black boxes of level 1 become white boxes.
We look into each component in detail.

==== IDS Integration

===== IoT Broker IDS Wrapper (Whitebox)

The IoT Broker IDS Wrapper handles the communication of the IoT Broker Service APIs with the IDS Connector.

The following table and figures provide an overview of the most relevant components.
For a complete overview, see the project's Javadoc documentation.

[cols="1,3",options="header"]
|===
|Component
|Description

|ResourceConfiguration
|Handles the creation and configuration of the necessary IDS resource at the IDS Connector.

|DeviceRegistryServiceController
|Offers REST endpoints to facilitate forwarding of IDS requests to the Device Registry Service.

|SensorDataHistoryServiceController
|Offers REST endpoints to facilitate forwarding of IDS requests to the Sensor Data History Service.
|===

.IoT Broker IDS Wrapper - Main components
image::images/05_iot_broker_ids_wrapper_main_components.png[]
