= Software documentation for the IoT Broker integration of the International Data Spaces (IDS)
:toc: left
:toclevels: 3
:sectnumlevels: 5
:icons: font
:copyright: Open Logistics Foundation

[NOTE]
.Before reading this documentation
====
This documentation _complements_ the arc42 documentation of the IoT Broker.
Therefore, sections that are already described in general in the arc42 documentation of the IoT Broker are not described here.
However, to conform to the general structure of the IoT Broker's arc42 documentation, the main sections are retained, even though there may not necessarily be corresponding content here for all of them.
This documentation therefore only contains content and descriptions for components that are specific to this project.
For detailed information on sections not described in this documentation and for general information about the IoT Broker, please refer to the link:https://git.openlogisticsfoundation.org/silicon-economy/base/iotbroker/iotbroker/-/blob/main/documentation/index.adoc[IoT Broker's arc42 documentation].
====

[[section-preface]]
== Preface

=== Main Authors

[cols="1,2",options="header"]
|===
|Name
|E-Mail

|Steffen Biehs
|mailto:steffen.biehs@isst.fraunhofer.de[]

|Johannes Pieperbeck
|mailto:johannes.pieperbeck@isst.fraunhofer.de[]

|Haydar Qarawlus
|mailto:haydar.qarawlus@isst.fraunhofer.de[]

|Ronja Quensel
|mailto:ronja.quensel@isst.fraunhofer.de[]

|Florian Zimmer
|mailto:florian.zimmer@isst.fraunhofer.de[]
|===

// Enable section numbering from here on
:sectnums:

<<<
// 1. Introduction and Goals
include::01_introduction_and_goals.adoc[]

<<<
// 2. Architecture Constraints
include::02_architecture_constraints.adoc[]

<<<
// 3. System Scope and Context
include::03_system_scope_and_context.adoc[]

<<<
// 4. Solution Strategy
include::04_solution_strategy.adoc[]

<<<
// 5. Building Block View
include::05_building_block_view.adoc[]

<<<
// 6. Runtime View
include::06_runtime_view.adoc[]

<<<
// 7. Deployment View
include::07_deployment_view.adoc[]

<<<
// 8. Concepts
include::08_concepts.adoc[]

<<<
// 9. Design Decisions
include::09_design_decisions.adoc[]

<<<
// 10. Quality Scenarios
include::10_quality_scenarios.adoc[]

<<<
// 11. Technical Risks
include::11_technical_risks.adoc[]

<<<
// 12. Tutorial
include::12_tutorial.adoc[]

<<<
// Glossary
include::glossary.adoc[]
