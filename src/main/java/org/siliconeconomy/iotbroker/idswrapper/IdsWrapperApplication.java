/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * IdsWrapperApplication.
 *
 * @author Malte Hellmeier
 */
@SpringBootApplication(scanBasePackages = {
    "org.siliconeconomy.idsintegrationtoolbox",
    "org.siliconeconomy.iotbroker.idswrapper"
})
public class IdsWrapperApplication {

    public static void main(String[] args) {
        SpringApplication.run(IdsWrapperApplication.class, args);
    }

}
