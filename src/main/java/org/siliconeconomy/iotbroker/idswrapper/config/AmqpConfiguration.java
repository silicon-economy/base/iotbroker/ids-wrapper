/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.support.converter.Jackson2JavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Contains the AMQP configuration.
 *
 * @author Ronja Quensel
 */
@Configuration
@RequiredArgsConstructor
public class AmqpConfiguration {
    
    /** Type precedence for mapping incoming messages. */
    public static final Jackson2JavaTypeMapper.TypePrecedence CONVERTER_TYPE_PRECEDENCE
            = Jackson2JavaTypeMapper.TypePrecedence.INFERRED;
    
    /** Whether to use a batch listener for incoming messages. */
    public static final boolean FACTORY_BATCH_LISTENER = true;
    
    /** Whether batching is enabled. */
    public static final boolean FACTORY_CONSUMER_BATCH_ENABLED = true;
    
    /** Whether debatching is enabled. */
    public static final boolean FACTORY_DEBATCHING_ENABLED = true;
    
    /** The batch size. */
    public static final int FACTORY_BATCH_SIZE = 5;
    
    /** The receive timeout. */
    public static final long FACTORY_RECEIVE_TIMEOUT = 1000L;
    
    /** Maps POJOs to/from JSON. */
    private final @NonNull ObjectMapper objectMapper;
    
    /**
     * Returns a Jackson2JsonMessageConverter with inferred type precedence.
     *
     * @return the Jackson2JsonMessageConverter.
     */
    @Bean
    public Jackson2JsonMessageConverter messageConverter() {
        final var converter = new Jackson2JsonMessageConverter(objectMapper);
        converter.setTypePrecedence(CONVERTER_TYPE_PRECEDENCE);
        return converter;
    }
    
    /**
     * Returns a SimpleRabbitListenerContainerFactory using the {@link #messageConverter()}. It
     * enables batch processing with a batch size of 5 and a receive timeout of 60 seconds.
     *
     * @param connectionFactory the connection factory.
     * @return the SimpleRabbitListenerContainerFactory.
     */
    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(
            ConnectionFactory connectionFactory) {
        var factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setBatchListener(FACTORY_BATCH_LISTENER);
        factory.setConsumerBatchEnabled(FACTORY_CONSUMER_BATCH_ENABLED);
        factory.setDeBatchingEnabled(FACTORY_DEBATCHING_ENABLED);
        factory.setBatchSize(FACTORY_BATCH_SIZE);
        factory.setReceiveTimeout(FACTORY_RECEIVE_TIMEOUT);
        factory.setMessageConverter(messageConverter());
    
        return factory;
    }
    
}
