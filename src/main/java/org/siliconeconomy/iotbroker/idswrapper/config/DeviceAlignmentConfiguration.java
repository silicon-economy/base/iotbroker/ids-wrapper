/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.config;

import javax.annotation.PostConstruct;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.iotbroker.idswrapper.service.DeviceAlignmentService;
import org.springframework.stereotype.Component;



/**
 * Class used to configure and start the device alignment service upon application startup.
 *
 * @author Haydar Qarawlus
 */
@Component
@RequiredArgsConstructor
@Log4j2
public class DeviceAlignmentConfiguration {

    /**
     * The device alignment service.
     */
    private final @NonNull DeviceAlignmentService deviceAlignmentService;
    
    /**
     * Starts the alignment automatically upon application startup.
     */
    @PostConstruct
    private void alignDevices() {
        log.info("Starting device alignment.");
        try {
            deviceAlignmentService.alignDevices();
        } catch (Exception e) {
            log.error("Unable to align devices: Exception caught. {}", e.getMessage());
        }
    }
}
