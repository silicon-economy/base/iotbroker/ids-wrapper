/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.siliconeconomy.iotbroker.idswrapper.subscription.deserialization.DeviceInstanceMixIn;
import org.siliconeconomy.iotbroker.idswrapper.subscription.deserialization.DeviceInstanceUpdateMixIn;
import org.siliconeconomy.iotbroker.idswrapper.subscription.deserialization.DeviceTypeMixIn;
import org.siliconeconomy.iotbroker.idswrapper.subscription.deserialization.DeviceTypeUpdateMixIn;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.siliconeconomy.iotbroker.sensordata.SensorDataModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Contains all configuration related to mapping between POJOs and JSON.
 *
 * @author Ronja Quensel
 */
@Configuration
public class MappingConfiguration {
    
    /**
     * Configures the ObjectMapper for this wrapper application. Registers the JavaTimeModule to
     * enable (de)serialization of dates as well as MixIn classes for IoT broker model classes.
     *
     * @return the configured ObjectMapper.
     */
    @Bean("iotBrokerWrapperObjectMapper")
    public ObjectMapper configureObjectMapper() {
        final var objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.registerModule(new SensorDataModule());
        objectMapper.addMixIn(DeviceTypeUpdate.class, DeviceTypeUpdateMixIn.class);
        objectMapper.addMixIn(DeviceType.class, DeviceTypeMixIn.class);
        objectMapper.addMixIn(DeviceInstanceUpdate.class, DeviceInstanceUpdateMixIn.class);
        objectMapper.addMixIn(DeviceInstance.class, DeviceInstanceMixIn.class);
        return objectMapper;
    }
    
}
