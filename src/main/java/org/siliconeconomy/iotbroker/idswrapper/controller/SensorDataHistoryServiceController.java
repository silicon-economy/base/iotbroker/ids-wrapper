/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.controller;

import java.util.Map;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.iotbroker.idswrapper.utils.Utils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * The REST Controller for connecting to the SensorDataHistoryService of the IoT Broker.
 *
 * @author Johannes Pieperbeck, Ronja Quensel
 */
@RestController
@RequestMapping("/history")
@RequiredArgsConstructor
@Log4j2
public class SensorDataHistoryServiceController {
    
    private static final String DEFAULT_START_TIME = "1970-01-01T00:00:00Z";
    private static final String DEFAULT_END_TIME = "9999-12-31T23:59:59.999999Z";
    private static final Integer DEFAULT_OFFSET = 0;
    private static final Integer DEFAULT_LIMIT = 100;
    private static final String DEFAULT_SORT = "asc";
    
    /** Base path for the IoT broker. */
    @Value("${broker.uri}")
    private String brokerUri;
    
    /** Endpoint to fetch a message by source and tenant. */
    public static final String SOURCE_TENANTS_ENDPOINT = "messages/sources/{source}/tenants"
            + "/{tenant}";
    
    /** Endpoint to fetch the latest message by source and tenant. */
    public static final String SOURCE_TENANTS_LATEST_ENDPOINT = "messages/sources"
            + "/{source}/tenants/{tenant}/latest";
    
    /** Sends HTTP requests. */
    private final @NonNull RestTemplate restTemplate = new RestTemplate();
    
    /**
     * Returns the data for the device instance identified by the given source and tenant. Returns
     * the latest data if no additional query parameters are provided. If parameters are provided,
     * history data is returned.
     *
     * @param source the source.
     * @param tenant the tenant.
     * @param startTime the start time of the interval to view history data from (optional).
     * @param endTime the end time of the interval to view history data from (optional).
     * @param offset the offset for history data entries (optional).
     * @param limit the limit for history data entries (optional).
     * @param sort the sort order for history data entries (optional).
     * @return the data.
     */
    @GetMapping("/{source}/{tenant}")
    public ResponseEntity<String> getDataBySourceAndTenant(
            @PathVariable String source,
            @PathVariable String tenant,
            @RequestParam(required = false) String startTime,
            @RequestParam(required = false) String endTime,
            @RequestParam(required = false) Integer offset,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) String sort) {
        if (startTime == null && endTime == null && offset == null && limit == null
                && sort == null) {
            // No query parameters specified by consumer => return latest data
            final var pathParameters = Map.of(
                    "source", source,
                    "tenant", tenant
            );
            
            final var url = Utils.buildUrl(brokerUri, SOURCE_TENANTS_LATEST_ENDPOINT,
                    pathParameters, Map.of());
            
            try {
                return restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(""),
                        String.class);
            } catch (RestClientException e) {
                log.info("Exception caught during exchange with backend.", e);
                return new ResponseEntity<>("Request to sensor data history service could not "
                        + "be sent.", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            // Query parameters specified by consumer => return history data
            final var queryParameters = Map.of(
                    "startTime", valueOrDefault(startTime, DEFAULT_START_TIME),
                    "endTime", valueOrDefault(endTime, DEFAULT_END_TIME),
                    "offset", valueOrDefault(offset, DEFAULT_OFFSET).toString(),
                    "limit", valueOrDefault(limit, DEFAULT_LIMIT).toString(),
                    "sort", valueOrDefault(sort, DEFAULT_SORT)
            );
            final var pathParameters = Map.of(
                    "source", source,
                    "tenant", tenant
            );
            
            final var url = Utils.buildUrl(brokerUri, SOURCE_TENANTS_ENDPOINT, pathParameters,
                    queryParameters);
            
            try {
                return restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(""),
                        String.class);
            } catch (RestClientException e) {
                log.info("Exception caught during exchange with backend.", e);
                return new ResponseEntity<>("Request to sensor data history service could not "
                        + "be sent.", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
    
    private <T> T valueOrDefault(T value, T defaultValue) {
        return (value == null ? defaultValue : value);
    }
    
}
