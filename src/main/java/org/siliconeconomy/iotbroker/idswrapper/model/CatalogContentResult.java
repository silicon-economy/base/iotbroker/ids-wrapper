/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.model;

import java.net.URI;
import java.util.HashSet;

import lombok.Data;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;

/**
 * Container object for DSC catalogs. Contains the catalog ID as well as all resources that are
 * part of the catalog.
 *
 * @author Haydar Qarawlus
 */
@Data
public class CatalogContentResult {

    /** ID of the catalog. */
    private URI catalogUri;
    
    /** The resources that are part of the catalog. */
    private HashSet<OfferedResourceOutput> catalogResources;
    
    /**
     * Constructs a CatalogContentResult with empty resource set.
     *
     * @param catalogUri ID of the catalog.
     */
    public CatalogContentResult(URI catalogUri) {
        this.catalogUri = catalogUri;
        this.catalogResources = new HashSet<>();
    }
}
