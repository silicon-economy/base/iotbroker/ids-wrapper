/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.service;

import static java.lang.String.format;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.Catalog;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.EntityOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.EmptyFieldException;
import org.siliconeconomy.iotbroker.idswrapper.model.CatalogContentResult;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 * Service class to align the device types and instances found in the IoT Broker with the
 * corresponding IDS elements (e.g. Catalogs, Resources) at the DSC.
 *
 * @author : Haydar Qarawlus
 */
@Service
@Log4j2
public class DeviceAlignmentService {

    /** Base path for broker. */
    @Value("${device-registry.url}")
    private String deviceRegistryUri;

    /** Endpoint to fetch device instances. */
    @Value("${device-registry.device-instances-endpoint}")
    private String deviceInstancesEndpoint;

    /** Endpoint to fetch device types. */
    @Value("${device-registry.device-types-endpoint}")
    private String deviceTypesEndpoint;

    /** List of IDS brokers where device instance resources should be registered. */
    @Value("#{'${ids.metadata-broker.urls}'.split(';')}")
    private List<URI> brokers;
    
    @Value("${device-registry.page-size:100}")
    private int registryPageSize;

    /** REST request handler to send requests to the IoT Broker backend. */
    private final @NonNull RestRequestHandler restRequestHandler;

    /** Maps API responses to the corresponding Java objects. */
    private final @NonNull ObjectMapper objectMapper;

    /** Main DSC Api Operator. Provides access to all DSC functionalities. */
    private final @NonNull DscOperator dscOperator;
    
    /** Service for mapping between device types/instances and DSC entities. */
    private final @NonNull DeviceMappingService mappingService;
    
    /** Service for creating complete resources from device instances. */
    private final @NonNull ResourceCreationService creationService;
    
    /** Filters devices and updates according to a whitelist. */
    private final @NonNull DeviceFilter deviceFilter;

    /**
     * Constructor for the DeviceAlignmentService.
     *
     * @param restRequestHandler REST request handler to send requests to the IoT Broker backend.
     * @param dscOperator Main DSC Api Operator. Provides access to all DSC functionalities.
     * @param mappingService Service for mapping between device types/instances and DSC entities.
     * @param creationService Service for creating complete resources from device instances.
     * @param objectMapper Maps API responses to the corresponding Java objects.
     */
    @Autowired
    public DeviceAlignmentService(@NonNull RestRequestHandler restRequestHandler,
                                  @NonNull DscOperator dscOperator,
                                  @NonNull DeviceMappingService mappingService,
                                  @NonNull ResourceCreationService creationService,
                                  @NonNull @Qualifier("iotBrokerWrapperObjectMapper")
                                          ObjectMapper objectMapper,
                                  @NonNull DeviceFilter deviceFilter) {
        this.restRequestHandler = restRequestHandler;
        this.objectMapper = objectMapper;
        this.dscOperator = dscOperator;
        this.mappingService = mappingService;
        this.creationService = creationService;
        this.deviceFilter = deviceFilter;
    }

    /**
     * Start the alignment of the DSC to the devices registered with the IoT Broker.
     *
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws IOException if mapping the input/output fails.
     * @throws EmptyFieldException if a required output field is missing.
     */
    public void alignDevices() throws ApiInteractionUnsuccessfulException, IOException,
            EmptyFieldException {
        // Get device types and instances from the IoT Broker
        final var deviceTypes = getDeviceTypes();
        final var deviceInstances = getDeviceInstances();
        
        // Get all resource catalogs from the Dataspace Connector
        final var originalResourceCatalogs = getCatalogsAndResources();
        
        // Clone the resource catalog map. Used for an intersection later to remove outdated
        // resources
        final var resourceCatalogs = new HashMap<String, CatalogContentResult>();
        
        // Match device instances to the device types and store in a map.
        final var filteredDevices = filterDeviceInstances(deviceTypes, deviceInstances);

        // Begin alignment by iterating over all device types
        for (var deviceType : deviceTypes) {
            // Get device identifier and description
            var deviceTypeIdentifier = deviceType.getIdentifier();

            // Check if device type does not exist in connector, create a new catalog
            if (!originalResourceCatalogs.containsKey(deviceTypeIdentifier)) {
                final var catalogOutput = createCatalog(deviceType);
                
                // Add catalog to resourceCatalog map to track created catalogs
                resourceCatalogs.put(
                        deviceTypeIdentifier,
                        new CatalogContentResult(
                                URI.create(catalogOutput.getLinks().getSelf().getHref())
                        ));
            } else {
                resourceCatalogs.put(deviceTypeIdentifier,
                        originalResourceCatalogs.get(deviceTypeIdentifier));
            }

            // Get the instances related to the device type
            final var instanceList = filteredDevices.get(deviceTypeIdentifier);

            // Get all device instances registered in the DSC for the device type.
            final var catalogContent = resourceCatalogs.get(deviceTypeIdentifier);
            
            // Loop over filtered/matched devices, compare and create resource if it does exist
            for (var deviceInstance : instanceList) {
                final Predicate<OfferedResourceOutput> titleEqualsId =
                        r -> r.getTitle().equals(deviceInstance.getId());
                if (catalogContent != null
                        && catalogContent.getCatalogResources().stream().noneMatch(titleEqualsId)) {
                    creationService.createResource(catalogContent.getCatalogUri(),
                            deviceInstance);
                }
            }
        }
        
        // Intersect original and updated resource catalog maps, remove non-intersecting
        // elements since they are now available in the DSC but not anymore in the IoT Broker.
        originalResourceCatalogs.keySet().removeAll(resourceCatalogs.keySet());

        log.info("Deleting remaining resources");
        
        // Iterate over the remaining resource after intersection and delete
        for (var originalResourceCatalog : originalResourceCatalogs.entrySet()) {
            final var catalogContentResult = originalResourceCatalog.getValue();
            deleteCatalogAndResource(catalogContentResult);
        }

        // Register the connector and all resources at the defined IDS brokers
        if (!brokers.isEmpty() && !brokers.get(0).toString().isEmpty()) {
            for (var broker : brokers) {
                dscOperator.workflows().broker().registerAllResources(broker);
            }
        }
    }

    /**
     * Match the device instances retrieved from the IoT Broker to their matching device type.
     *
     * @param deviceTypes the array of device types obtained from the IoT Broker.
     * @param deviceInstances the array of device instances obtained from the IoT Broker.
     *
     * @return a map containing the device type identifies as key and an array of matching device
     *     instances as value.
     */
    @SuppressWarnings("java:S3958") // false positive, toList() is a terminal stream operation
    private Map<String, List<DeviceInstance>> filterDeviceInstances(
            final List<DeviceType> deviceTypes, final List<DeviceInstance> deviceInstances) {
        final var filteredMap = new HashMap<String, List<DeviceInstance>>();
        
        for (var deviceType : deviceTypes) {
            final Predicate<DeviceInstance> instanceIsOfType =
                    i -> i.getDeviceTypeIdentifier().equals(deviceType.getIdentifier());
            
            filteredMap.putIfAbsent(deviceType.getIdentifier(),
                    (deviceInstances.stream()
                            .filter(instanceIsOfType)
                            .toList()));
        }
        
        return filteredMap;
    }

    /**
     * Communicates with IoT Broker Device Registry Service to retrieve device types
     * and convert into objects.
     *
     * @return Array of device types.
     * @throws ApiInteractionUnsuccessfulException in case of communication/deserialization errors.
     */
    private List<DeviceType> getDeviceTypes() throws ApiInteractionUnsuccessfulException {
        try {
            var types = getInfoFromBroker(deviceTypesEndpoint, DeviceType.class);
    
            return deviceFilter.filterTypes(types);
        } catch (Exception e) {
            throw new ApiInteractionUnsuccessfulException(
                    format("Unable to retrieve device types: %s ", e.getMessage()), e);
        }
    }

    /**
     * Communicates with IoT Broker Device Registry Service to retrieve device types
     * and convert into objects.
     *
     * @return Array of device instances.
     * @throws ApiInteractionUnsuccessfulException in case of communication/deserialization errors.
     */
    private List<DeviceInstance> getDeviceInstances() throws ApiInteractionUnsuccessfulException {
        try {
            var instances = getInfoFromBroker(deviceInstancesEndpoint, DeviceInstance.class);
            
            return deviceFilter.filterInstances(instances);
        } catch (Exception e) {
            throw new ApiInteractionUnsuccessfulException(
                    format("Unable to retrieve device instances: %s", e.getMessage()), e);

        }
    }
    
    /**
     * Helper method for communication with IoT Broker Device Registry Service. Makes requests to
     * retrieve an entity until no more entities are returned.
     *
     * @param endpoint the exact endpoint to be communicated with.
     * @param type the type of entity to fetch.
     * @return The entities retrieved from the IoT Broker
     * @throws ApiInteractionUnsuccessfulException if unable to communicate with the IoT Broker
     *                                             backend.
     */
    @SuppressWarnings("unchecked")
    private <T> List<T> getInfoFromBroker(final String endpoint, Class<T> type)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        log.info("Sending request to device registry at {}/{}. Retrieving instances.",
                deviceRegistryUri, endpoint);
    
        final var entities = new ArrayList<T>();
        var moreAvailable = true;
        var pageIndex = 0;
        do {
            final var requestBuilder = new RestRequestBuilder(HttpMethod.GET, deviceRegistryUri,
                    endpoint)
                    .queryParameter("pageSize", String.valueOf(registryPageSize))
                    .queryParameter("pageIndex", String.valueOf(pageIndex));
            final var response = restRequestHandler.sendRequest(requestBuilder.build());
            var typeReference =
                    TypeFactory.defaultInstance().constructCollectionType(List.class, type);
            final var entries = (List<T>) objectMapper.readValue(response, typeReference);
            entities.addAll(entries);

            pageIndex += 1;

            if (entries.size() < registryPageSize) {
                moreAvailable = false;
            }
        } while (moreAvailable);
        
        log.info("Retrieved {} instances in total.", entities.size());
        
        return entities;
    }
    
    /**
     * Query all catalogs and offered resources from the connector and pack them in suitable
     * objects.
     *
     * @return a map containing the catalog title as key and the contents of the catalog as value.
     *
     * @throws ApiInteractionUnsuccessfulException if unable to communicate with the DSC.
     * @throws JsonProcessingException if unable to process the response obtained from the DSC.
     * @throws EmptyFieldException if unable to extract the UUID from the catalog URI.
     */
    private Map<String, CatalogContentResult> getCatalogsAndResources()
            throws ApiInteractionUnsuccessfulException, JsonProcessingException,
            EmptyFieldException {
        // Get all catalogs from the connector
        var connectorCatalogs = dscOperator.entities()
                .catalogs()
                .getAll()
                .getEmbedded()
                .getEntries();

        // Create an empty hash map to store the catalog and resource results
        final var catalogResourceMap = new HashMap<String, CatalogContentResult>();
        for (var catalog : connectorCatalogs) {
            final var catalogUuid = UuidUtils.getUuidFromOutput(catalog);
            
            // Create an object containing the URI of the catalog an
            final var catalogContentResult = new CatalogContentResult(
                    URI.create(catalog.getLinks()
                            .getSelf()
                            .getHref()
                    ));

            // Add a new entry in the map with the catalog title as key and the resources object
            // as value
            catalogResourceMap.putIfAbsent(catalog.getTitle(),
                    catalogContentResult);

            // Get the offered resource of a catalog
            final var catalogOfferedResources = dscOperator
                    .entities()
                    .catalogs()
                    .offers()
                    .getAll(catalogUuid)
                    .getEmbedded()
                    .getEntries();

            // Iterate through the resources offered in the catalog, add them to the map.
            for (var offeredResourceOutput : catalogOfferedResources) {

                catalogResourceMap
                        .get(catalog.getTitle())
                        .getCatalogResources()
                        .add(offeredResourceOutput);
            }
        }

        return catalogResourceMap;
    }

    /**
     * Create a new catalog with the deviceTypeIdentifier as title and deviceType description as
     * catalog description.
     *
     * @param deviceType the device type.
     * @return the catalog output returned after creation at the DSC.
     * @throws ApiInteractionUnsuccessfulException if unable to communicate with the DSC.
     * @throws JsonProcessingException if unable to (de-)serialize the communication with the DSC.
     */
    private EntityOutput<Catalog> createCatalog(final DeviceType deviceType)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var catalogInput = mappingService.deviceTypeToCatalog(deviceType);
        return dscOperator
                .entities()
                .catalogs()
                .create(catalogInput);
    }
    
    /**
     * Delete the resources contained in a catalog. Delete the catalog as well afterwards.
     *
     * @param catalogContentResult the object containing the catalogURI and its offered resources.
     *
     * @throws EmptyFieldException if unable to extract UUID from catalog URI.
     * @throws ApiInteractionUnsuccessfulException if unable to communicate with the DSC.
     * @throws JsonProcessingException if unable to process the DSC response.
     */
    private void deleteCatalogAndResource(final CatalogContentResult catalogContentResult)
            throws EmptyFieldException, ApiInteractionUnsuccessfulException,
            JsonProcessingException {
        final var remainingCatalogResources = catalogContentResult.getCatalogResources();

        // Iterate over all the resources in the catalog
        for (var offeredResourceOutput : remainingCatalogResources) {

            dscOperator.workflows()
                    .resourceRemoval()
                    .deleteResourceAndCatalog(offeredResourceOutput);
        }
    }
}
