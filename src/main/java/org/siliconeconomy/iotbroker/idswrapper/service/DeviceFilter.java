/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.service;

import java.util.List;
import java.util.stream.Collectors;

import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Filters device types, device instances and updates received via AMQP according to a whitelist.
 *
 * @author Ronja Quensel
 */
@Component
public class DeviceFilter {
    
    /** Whether device filtering is enabled. If set to false, all devices are published. */
    @Value("${device-filter.enabled:true}")
    private boolean deviceFilterEnabled;
    
    /** Whitelist for device sources. Only regarded if device filtering is enabled. */
    @Value("#{'${device-filter.allowed-sources}'.split(';')}")
    private List<String> allowedDeviceSources;
    
    /**
     * Filters device types. If filtering is enabled, removes all types where the source is not
     * contained in the whitelist.
     *
     * @param deviceTypes the types to filter.
     * @return the filtered list.
     */
    public List<DeviceType> filterTypes(final List<DeviceType> deviceTypes) {
        if (deviceFilterEnabled) {
            return deviceTypes.stream()
                    .filter(t -> allowedDeviceSources.contains(t.getSource()))
                    .collect(Collectors.toList());
        }
        
        return deviceTypes;
    }
    
    /**
     * Filters device instances. If filtering is enabled, removes all instances where the source
     * is not contained in the whitelist.
     *
     * @param deviceInstances the instances to filter.
     * @return the filtered list.
     */
    public List<DeviceInstance> filterInstances(final List<DeviceInstance> deviceInstances) {
        if (deviceFilterEnabled) {
            return deviceInstances.stream()
                    .filter(i -> allowedDeviceSources.contains(i.getSource()))
                    .collect(Collectors.toList());
        }
        
        return deviceInstances;
    }
    
    /**
     * Checks whether processing a SensorDataMessage is allowed by the whitelist. If filtering is
     * enabled, checks whether the message's source is contained in the whitelist.
     *
     * @param sensorDataMessage the message.
     * @return true, if the message may be processed; false otherwise.
     */
    public boolean isProcessingAllowed(SensorDataMessage sensorDataMessage) {
        return !deviceFilterEnabled
                || allowedDeviceSources.contains(sensorDataMessage.getSource());
    }
    
    /**
     * Checks whether processing a DeviceTypeUpdate is allowed by the whitelist. If filtering is
     * enabled, checks whether the source of old and new state is contained in the whitelist.
     *
     * @param deviceTypeUpdate the update.
     * @return true, if the update may be processed; false otherwise.
     */
    public boolean isProcessingAllowed(DeviceTypeUpdate deviceTypeUpdate) {
        var oldSource = deviceTypeUpdate.getOldState().getSource();
        var newSource = deviceTypeUpdate.getNewState().getSource();
        return deviceSourceWhitelisted(oldSource, newSource);
    }
    
    /**
     * Checks whether processing a DeviceInstanceUpdate is allowed by the whitelist. If filtering is
     * enabled, checks whether the source of old and new state is contained in the whitelist.
     *
     * @param deviceInstanceUpdate the update.
     * @return true, if the update may be processed; false otherwise.
     */
    public boolean isProcessingAllowed(DeviceInstanceUpdate deviceInstanceUpdate) {
        var oldSource = deviceInstanceUpdate.getOldState().getSource();
        var newSource = deviceInstanceUpdate.getNewState().getSource();
        return deviceSourceWhitelisted(oldSource, newSource);
    }
    
    /**
     * Checks whether the old and new source of an update are allowed to be processed.
     *
     * @param oldSource the source of the old state.
     * @param newSource the source of the new state.
     * @return true, if processing is allowed; false otherwise.
     */
    private boolean deviceSourceWhitelisted(final String oldSource, final String newSource) {
        if (deviceFilterEnabled) {
            return (allowedDeviceSources.contains(oldSource)
                    && allowedDeviceSources.contains(newSource));
        }
        
        return true;
    }
    
}
