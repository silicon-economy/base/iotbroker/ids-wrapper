/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PaymentMethod;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.CatalogInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RepresentationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RuleInput;
import org.siliconeconomy.idsintegrationtoolbox.utils.PolicyUtils;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Service for mapping between DSC entities and device types/instances.
 *
 * @author Ronja Quensel
 */
@Service
public class DeviceMappingService {
    
    /** URL of this wrapper application. */
    @Value("${wrapper.uri}")
    private String wrapperUrl;
    
    /** Port on which this wrapper application is running. */
    @Value("${server.port}")
    private String serverPort;
    
    /** Serializes IDS Information Model objects to JSON-LD. */
    private final Serializer serializer;
    
    /**
     * Constructs a DeviceMappingService.
     */
    @Autowired
    public DeviceMappingService() {
        this.serializer = new Serializer();
    }
    
    /**
     * Creates a catalog input from a device type.
     *
     * @param deviceType the device type.
     * @return the catalog input.
     */
    public CatalogInput deviceTypeToCatalog(final DeviceType deviceType) {
        final var catalogInput = new CatalogInput();
        catalogInput.setTitle(deviceType.getIdentifier());
        catalogInput.setDescription(deviceType.getDescription());
        return catalogInput;
    }
    
    /**
     * Creates a resource input from a device instance.
     *
     * @param deviceInstance the device instance.
     * @return the resource input.
     */
    public OfferedResourceInput deviceInstanceToResource(final DeviceInstance deviceInstance) {
        final var resourceInput = new OfferedResourceInput();
        resourceInput.setTitle(deviceInstance.getId());
        resourceInput.setDescription(deviceInstance.getDescription());
        resourceInput.setKeywords(List.of(deviceInstance.getSource(), deviceInstance.getTenant()));
        resourceInput.setLanguage("en");
        resourceInput.setPaymentMethod(PaymentMethod.UNDEFINED);
        return resourceInput;
    }
    
    /**
     * Creates a representation input from a device instance.
     *
     * @param deviceInstance the device instance.
     * @return the representation input.
     */
    public RepresentationInput deviceInstanceToRepresentation(final DeviceInstance deviceInstance) {
        final var representationInput = new RepresentationInput();
        representationInput.setTitle(deviceInstance.getId());
        representationInput.setMediaType("json");
        return representationInput;
    }
    
    /**
     * Creates a artifact input from a device instance.
     *
     * @param deviceInstance the device instance.
     * @return the artifact input.
     */
    public ArtifactInput deviceInstanceToArtifact(final DeviceInstance deviceInstance)
            throws MalformedURLException {
        final var urlString = String.format("%s:%s/history/%s/%s",
                wrapperUrl,
                serverPort,
                deviceInstance.getSource(),
                deviceInstance.getTenant());
        final var accessUrl = new URL(urlString);
    
        final var artifactInput = new ArtifactInput();
        artifactInput.setTitle(deviceInstance.getId());
        artifactInput.setAccessUrl(accessUrl);
        return artifactInput;
    }
    
    /**
     * Creates a rule input with a provide-access-rule.
     *
     * @return the rule input.
     * @throws IOException if serializing the rule fails.
     */
    public RuleInput getProvideAccessRule() throws IOException {
        final var idsRule = PolicyUtils.buildProvideAccessRule("Provide access");
        final var serialized = serializer.serialize(idsRule);
        return new RuleInput(serialized);
    }
    
}
