/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.service;

import java.io.IOException;
import java.net.URI;
import java.time.ZonedDateTime;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ContractInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Service that creates the resources for device instances.
 *
 * @author Ronja Quensel
 */
@Service
@RequiredArgsConstructor
public class ResourceCreationService {

    /** Contract title for device instance resources. */
    @Value("${device-instance.contract.title:IoT Broker Device Contract}")
    private String contractTitle;
    
    /** Operator for the Dataspace Connector API. */
    private final @NonNull DscOperator dscOperator;
    
    /** Service for mapping between device types/instances and DSC entities. */
    private final @NonNull DeviceMappingService mappingService;
    
    /**
     * Creates a resource with representation and artifact from a device instance in a given
     * catalog. Uses the existing contract for device instances, if one exists. Otherwise, creates
     * the contract including a provide-access-rule.
     *
     * @param catalogUri ID of the catalog.
     * @param deviceInstance the device instance to map
     * @return the created resource.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws IOException if an error occurs mapping the input or response to/from JSON.
     */
    public OfferedResourceOutput createResource(final URI catalogUri,
                                                final DeviceInstance deviceInstance)
            throws ApiInteractionUnsuccessfulException, IOException {
        // Map device instance to resource, representation and artifact
        final var resourceInput = mappingService.deviceInstanceToResource(deviceInstance);
        final var representationInput = mappingService
                .deviceInstanceToRepresentation(deviceInstance);
        final var artifactInput = mappingService.deviceInstanceToArtifact(deviceInstance);
        
        // Find contract for device instance resources, if it exists
        var contract = dscOperator.entities().contracts().getAll()
                .getEmbedded().getEntries().stream()
                .filter(c -> c.getTitle().equals(contractTitle))
                .findFirst().orElse(null);

        // Create complete resource with the existing or a new contract
        if (contract == null) {
            var contractInput = new ContractInput(contractTitle, ZonedDateTime.now().plusYears(3));
            var ruleInput = mappingService.getProvideAccessRule();
            return dscOperator.workflows().resourceCreation().createResource(resourceInput,
                    catalogUri, representationInput, artifactInput,
                    contractInput, ruleInput);
        } else {
            var contractUri = URI.create(contract.getLinks().getSelf().getHref());
            return dscOperator.workflows().resourceCreation().createResource(resourceInput,
                    catalogUri, representationInput, artifactInput, contractUri);
        }
    }
    
}
