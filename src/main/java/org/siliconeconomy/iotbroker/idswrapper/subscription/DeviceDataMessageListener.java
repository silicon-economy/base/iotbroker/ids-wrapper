/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.subscription;

import static java.lang.String.format;

import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.iotbroker.amqp.SensorDataExchangeConfiguration;
import org.siliconeconomy.iotbroker.idswrapper.service.DeviceFilter;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Service that listens to device instance data updates via AMQP. When an update is received,
 * the service triggers IDS notifications to all subscribed connectors.
 *
 * @author Ronja Quensel
 */
@Service
@Log4j2
public class DeviceDataMessageListener {
    
    /** Operator for the Dataspace Connector API. */
    private final @NonNull DscOperator dscOperator;
    
    /** Maps between POJOs and JSON. */
    private final @NonNull ObjectMapper objectMapper;
    
    /** Filters devices and updates according to a whitelist. */
    private final @NonNull DeviceFilter deviceFilter;
    
    /**
     * Constructs a DeviceDataMessageListener.
     *
     * @param dscOperator operator for the Dataspace Connector API.
     * @param objectMapper maps between POJOs and JSON.
     * @param deviceFilter filters devices and updates according to a whitelist.
     */
    public DeviceDataMessageListener(@NonNull DscOperator dscOperator,
                                     @NonNull @Qualifier("iotBrokerWrapperObjectMapper")
                                             ObjectMapper objectMapper,
                                     @NonNull DeviceFilter deviceFilter) {
        this.dscOperator = dscOperator;
        this.objectMapper = objectMapper;
        this.deviceFilter = deviceFilter;
    }
    
    /**
     * Receives data updates for device instance data via AMQP. When an update is received,
     * finds the artifact corresponding to the device instance and triggers IDS notifications
     * for this artifact via the DSC.
     *
     * @param messages the received messages.
     * @param channel the channel.
     */
    @RabbitListener(
            bindings = @QueueBinding(
                    value = @Queue(name = "", durable = "true"),
                    exchange = @Exchange(
                            name = SensorDataExchangeConfiguration.NAME,
                            type = SensorDataExchangeConfiguration.TYPE,
                            ignoreDeclarationExceptions = "true"
                    ),
                    key = SensorDataExchangeConfiguration.ROUTING_KEY
            )
    )
    public void processDeviceDataUpdates(final List<Message> messages,
                                         final Channel channel) {
        for (var message : messages) {
            String messageId = null;

            try {
                final var sensorDataMessage = objectMapper
                        .readValue(message.getBody(), SensorDataMessage.class);
                
                // only process update if device source is white-listed
                if (!deviceFilter.isProcessingAllowed(sensorDataMessage)) {
                    continue;
                }
                
                messageId = sensorDataMessage.getId();
                final var source = sensorDataMessage.getSource();
                final var tenant = sensorDataMessage.getTenant();
                
                // Find resource corresponding to device instance that sent data
                var resource = findResourceBySourceAndTenant(source, tenant);
    
                // If resource found, trigger IDS notifications for resource
                if (resource != null) {
                    var resourceUri = new URI(resource.getLinks().getSelf().getHref());
                    dscOperator.ids().messages().notify(resourceUri);
                    
                    log.info(format("Received data update message %s for source %s and tenant %s."
                            + " Sent IDS notifications for resource %s.", messageId, source, tenant,
                            resourceUri));
                } else {
                    log.error(format("Failed to send IDS notifications for sensor data message."
                            + " [messageId=%s, reason=No corresponding resource found for device"
                                    + " instance.]", sensorDataMessage.getId()));
                }
            } catch (Exception e) {
                log.error(format("Failed to send IDS notifications for sensor data message."
                        + " [messageId=%s, reason=%s]", messageId, e.getMessage()));
            }
        }
    }
    
    /**
     * Finds and returns the resource corresponding to a given source and tenant. Source and
     * tenant uniquely identify a device instance. Returns null, if no corresponding resource is
     * found.
     *
     * @param source the device instance source.
     * @param tenant the device instance tenant.
     * @return the resource, if it exists; null otherwise.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if parsing the output fails.
     */
    private OfferedResourceOutput findResourceBySourceAndTenant(final String source,
                                                                final String tenant)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var resources = dscOperator.entities().resources().offers().getAll();
        
        // Loop over all offered resources
        for (var resource : resources.getEmbedded().getEntries()) {
            final var keywords = resource.getKeywords();
            
            // Find resource by source and tenant in keywords
            if (keywords.containsAll(List.of(source, tenant))) {
                return resource;
            }
        }
        
        return null;
    }
}
