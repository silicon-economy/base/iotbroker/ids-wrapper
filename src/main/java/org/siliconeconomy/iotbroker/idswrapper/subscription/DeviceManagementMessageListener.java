/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.subscription;

import static java.lang.String.format;
import static org.siliconeconomy.iotbroker.idswrapper.utils.Utils.identifierFromRoutingKey;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.EmptyFieldException;
import org.siliconeconomy.iotbroker.amqp.DeviceManagementExchangeConfiguration;
import org.siliconeconomy.iotbroker.idswrapper.service.DeviceFilter;
import org.siliconeconomy.iotbroker.idswrapper.service.DeviceMappingService;
import org.siliconeconomy.iotbroker.idswrapper.service.ResourceCreationService;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Service that listens to device type and instance updates via AMQP. When an update is received,
 * the service creates or updates the respective catalog or resource.
 *
 * @author Ronja Quensel
 */
@Service
@Log4j2
public class DeviceManagementMessageListener {
    
    private static final String RECEIVED_INSTANCE_UPDATE_LOG = "Received update notification for"
            + " device instance with ID %s of type %s, but no corresponding %s was found.";

    /** Operator for the Dataspace Connector API. */
    private final @NonNull DscOperator dscOperator;
    
    /** Service for mapping between device types/instances and DSC entities. */
    private final @NonNull DeviceMappingService mappingService;
    
    /** Service for creating complete resources from device instances. */
    private final @NonNull ResourceCreationService creationService;
    
    /** Maps between POJOs and JSON. */
    private final @NonNull ObjectMapper objectMapper;
    
    /** Filters devices and updates according to a whitelist. */
    private final @NonNull DeviceFilter deviceFilter;
    
    /** List of IDS brokers where device instance resources should be registered. */
    @Value("#{'${ids.metadata-broker.urls}'.split(';')}")
    private List<URI> brokers;
    
    /**
     * Constructs a DeviceManagementMessageListener.
     *
     * @param dscOperator operator for the Dataspace Connector API.
     * @param mappingService service for mapping between device types/instances and DSC entities.
     * @param creationService service for creating complete resources from device instances.
     * @param objectMapper maps between POJOs and JSON.
     */
    public DeviceManagementMessageListener(@NonNull DscOperator dscOperator,
                                           @NonNull DeviceMappingService mappingService,
                                           @NonNull ResourceCreationService creationService,
                                           @NonNull @Qualifier("iotBrokerWrapperObjectMapper")
                                                   ObjectMapper objectMapper,
                                           @NonNull DeviceFilter deviceFilter) {
        this.dscOperator = dscOperator;
        this.mappingService = mappingService;
        this.creationService = creationService;
        this.objectMapper = objectMapper;
        this.deviceFilter = deviceFilter;
    }

    /**
     * Receives device type updates via AMQP. Depending on the update type, either creates, updates
     * or deletes a device type catalog in the DSC.
     *
     * @param messages the received messages.
     * @param channel the channel.
     */
    @RabbitListener(
            bindings = @QueueBinding(
                    value = @Queue(name = "", durable = "true"),
                    exchange = @Exchange(
                            name = DeviceManagementExchangeConfiguration.NAME,
                            type = DeviceManagementExchangeConfiguration.TYPE,
                            ignoreDeclarationExceptions = "true"
                    ),
                    key = "deviceType.*.update"
            )
    )
    public void processDeviceTypeUpdate(final List<Message> messages, final Channel channel) {
        for (var message : messages) {
            final var routingKey = message.getMessageProperties().getReceivedRoutingKey();
            
            try {
                // Parse message body and get update type
                final var deviceTypeUpdate = objectMapper
                        .readValue(message.getBody(), DeviceTypeUpdate.class);
    
                // only process update if device source is white-listed
                if (!deviceFilter.isProcessingAllowed(deviceTypeUpdate)) {
                    continue;
                }
                
                final var updateType = deviceTypeUpdate.getType();

                // Execute respective action depending on update type
                switch (updateType) {
                    case CREATED -> createDeviceType(deviceTypeUpdate);
                    case MODIFIED -> updateDeviceType(deviceTypeUpdate);
                    case DELETED -> deleteDeviceType(deviceTypeUpdate);
                    default -> log.warn(format("Received unknown update type for device"
                            + " type: %s", updateType));
                }
                
                log.info(format("Processed device type update %s for %s.", updateType,
                        identifierFromRoutingKey(routingKey)));
            } catch (IOException | ApiInteractionUnsuccessfulException | EmptyFieldException
                    | IllegalStateException e) {
                log.error(format("Failed to process device type update."
                                + " [Identifier=%s, reason=%s]",
                        identifierFromRoutingKey(routingKey), e.getMessage()));
            }
        }
    }
    
    /**
     * Creates a new device type catalog from a {@link DeviceTypeUpdate}.
     *
     * @param deviceTypeUpdate holds the update information.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if an error occurs mapping the input or response.
     */
    private void createDeviceType(final DeviceTypeUpdate deviceTypeUpdate)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        final var newDeviceType = deviceTypeUpdate.getNewState();
        final var catalogInput = mappingService.deviceTypeToCatalog(newDeviceType);
        dscOperator.entities().catalogs().create(catalogInput);
    
        // Update connector at IDS brokers
        updateBrokerRegistration();
    }
    
    /**
     * Updates an existing device type catalog from a {@link DeviceTypeUpdate}.
     *
     * @param deviceTypeUpdate holds the update information.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if an error occurs mapping the input or response.
     * @throws EmptyFieldException if an ID cannot be read.
     * @throws IllegalStateException if the device type catalog does not exist.
     */
    private void updateDeviceType(final DeviceTypeUpdate deviceTypeUpdate)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException,
            EmptyFieldException {
        final var updatedDeviceType = deviceTypeUpdate.getNewState();
        final var existingCatalog = findDeviceTypeCatalog(updatedDeviceType.getIdentifier());
        
        if (existingCatalog != null) {
            final var catalogId = UuidUtils.getUuidFromOutput(existingCatalog);
            final var catalogInput = mappingService.deviceTypeToCatalog(updatedDeviceType);
            dscOperator.entities().catalogs().update(catalogId, catalogInput);
    
            // Update connector at IDS brokers
            updateBrokerRegistration();
        } else {
            throw new IllegalStateException(format("Received update notification for device"
                    + " type %s, but no corresponding catalog was found.",
                    updatedDeviceType.getIdentifier()));
        }
    }
    
    /**
     * Deletes an existing device type catalog from a {@link DeviceTypeUpdate}.
     *
     * @param deviceTypeUpdate holds the update information.
     */
    private void deleteDeviceType(final DeviceTypeUpdate deviceTypeUpdate)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException,
            EmptyFieldException {
        var deviceType = deviceTypeUpdate.getOldState();
        final var existingCatalog = findDeviceTypeCatalog(deviceType.getIdentifier());

        // Delete the catalog and all resources it contains
        if (existingCatalog != null) {
            dscOperator.workflows().resourceRemoval().deleteCatalog(existingCatalog);
    
            // Update connector at IDS brokers
            updateBrokerRegistration();
        }
    }
    
    /**
     * Receives device instance updates via AMQP. Depending on the update type, either creates,
     * updates or deletes a device instance resource in the DSC.
     *
     * @param messages the received messages.
     * @param channel the channel.
     */
    @RabbitListener(
            bindings = @QueueBinding(
                    value = @Queue(name = "", durable = "true"),
                    exchange = @Exchange(
                            name = DeviceManagementExchangeConfiguration.NAME,
                            type = DeviceManagementExchangeConfiguration.TYPE,
                            ignoreDeclarationExceptions = "true"
                    ),
                    key = "deviceInstance.*.update"
            )
    )
    public void processDeviceInstanceUpdate(final List<Message> messages, final Channel channel) {
        for (var message : messages) {
            final var routingKey = message.getMessageProperties().getReceivedRoutingKey();
            
            try {
                // Parse message body and get update type
                final var deviceInstanceUpdate = objectMapper
                        .readValue(message.getBody(), DeviceInstanceUpdate.class);
    
                // only process update if device source is white-listed
                if (!deviceFilter.isProcessingAllowed(deviceInstanceUpdate)) {
                    continue;
                }
                
                final var updateType = deviceInstanceUpdate.getType();
    
                // Execute respective action depending on update type
                switch (updateType) {
                    case CREATED -> createDeviceInstance(deviceInstanceUpdate);
                    case MODIFIED -> updateDeviceInstance(deviceInstanceUpdate);
                    case DELETED -> deleteDeviceInstance(deviceInstanceUpdate);
                    default -> log.warn(format("Received unknown update type for device"
                            + " instance: %s", updateType));
                }
    
                log.info(format("Processed device instance update %s for type %s.", updateType,
                        identifierFromRoutingKey(routingKey)));
            } catch (Exception e) {
                log.error(format("Failed to process device instance update."
                                + " [type ID=%s, reason=%s]",
                        identifierFromRoutingKey(routingKey), e.getMessage()));
            }
        }
    }
    
    /**
     * Creates a new device instance resource from a {@link DeviceInstanceUpdate}.
     *
     * @param deviceInstanceUpdate holds the update information.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws IOException if an error occurs mapping the input or response.
     * @throws IllegalStateException if the corresponding device type catalog does not exist.
     */
    private void createDeviceInstance(final DeviceInstanceUpdate deviceInstanceUpdate)
            throws ApiInteractionUnsuccessfulException, IOException {
        final var newDeviceInstance = deviceInstanceUpdate.getNewState();
        final var typeIdentifier = newDeviceInstance.getDeviceTypeIdentifier();
        
        final var existingCatalog = findDeviceTypeCatalog(typeIdentifier);
        if (existingCatalog != null) {
            var catalogUri = URI.create(existingCatalog.getLinks().getSelf().getHref());
            var resource = creationService.createResource(catalogUri, newDeviceInstance);

            // Register new resource at IDS brokers
            var resourceUri = URI.create(resource.getLinks().getSelf().getHref());
            updateBrokerRegistration(resourceUri);
        } else {
            throw new IllegalStateException(format("Cannot create device instance with"
                            + " ID %s. No matching device type catalog found for device type"
                            + " identifier %s.",
                    newDeviceInstance.getId(), typeIdentifier));
        }
    }
    
    /**
     * Updates an existing device instance resource including representation and artifact from a
     * {@link DeviceInstanceUpdate}. Will not update if resource, representation or artifact
     * are missing.
     *
     * @param deviceInstanceUpdate holds the update information.
     * @throws EmptyFieldException if an ID cannot be read.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if an error occurs mapping the input or response.
     * @throws MalformedURLException if the artifact access URL cannot be created.
     * @throws IllegalStateException if the device instance resource, representation or artifact
     *                               does not exist.
     */
    private void updateDeviceInstance(final DeviceInstanceUpdate deviceInstanceUpdate)
            throws EmptyFieldException, ApiInteractionUnsuccessfulException,
            JsonProcessingException, MalformedURLException {
        final var updatedDeviceInstance = deviceInstanceUpdate.getNewState();
        
        // Find resource for device instance
        var resource = findDeviceInstanceResource(updatedDeviceInstance.getId(),
                updatedDeviceInstance.getDeviceTypeIdentifier());
        if (resource == null) {
            throw new IllegalStateException(format(RECEIVED_INSTANCE_UPDATE_LOG,
                    updatedDeviceInstance.getId(), updatedDeviceInstance.getDeviceTypeIdentifier(),
                    "resource"));
        }

        final var resourceId = UuidUtils.getUuidFromOutput(resource);
        
        // Find corresponding representation
        var representation = dscOperator.entities()
                .resources()
                .offers()
                .representations()
                .getAll(resourceId)
                .getEmbedded().getEntries().stream()
                .findFirst()
                .orElse(null);
        if (representation == null) {
            throw new IllegalStateException(format(RECEIVED_INSTANCE_UPDATE_LOG,
                    updatedDeviceInstance.getId(), updatedDeviceInstance.getDeviceTypeIdentifier(),
                    "representation"));
        }

        var representationId = UuidUtils.getUuidFromOutput(representation);
        
        // Find corresponding artifact
        final var artifact = dscOperator.entities()
                .representations()
                .artifacts()
                .getAll(representationId)
                .getEmbedded().getEntries().stream()
                .findFirst()
                .orElse(null);
        if (artifact == null) {
            throw new IllegalStateException(format(RECEIVED_INSTANCE_UPDATE_LOG,
                    updatedDeviceInstance.getId(), updatedDeviceInstance.getDeviceTypeIdentifier(),
                    "artifact"));
        }
    
        // Update resource
        final var resourceInput = mappingService.deviceInstanceToResource(updatedDeviceInstance);
        dscOperator.entities().resources().offers().update(resourceId, resourceInput);

        // Update representation
        final var representationInput = mappingService
                .deviceInstanceToRepresentation(updatedDeviceInstance);
        dscOperator.entities().representations().update(representationId, representationInput);

        // Update artifact
        final var artifactInput = mappingService.deviceInstanceToArtifact(updatedDeviceInstance);
        final var artifactId = UuidUtils.getUuidFromOutput(artifact);
        dscOperator.entities().artifacts().update(artifactId, artifactInput);

        // Register updated resource at IDS brokers
        var resourceUri = URI.create(resource.getLinks().getSelf().getHref());
        updateBrokerRegistration(resourceUri);
    }
    
    /**
     * Deletes an existing device instance resource from a {@link DeviceInstanceUpdate}.
     *
     * @param deviceInstanceUpdate holds the update information.
     */
    private void deleteDeviceInstance(final DeviceInstanceUpdate deviceInstanceUpdate)
            throws ApiInteractionUnsuccessfulException, EmptyFieldException,
            JsonProcessingException {
        final var deviceInstance = deviceInstanceUpdate.getOldState();

        // Find resource for device instance
        var resource = findDeviceInstanceResource(deviceInstance.getId(),
                deviceInstance.getDeviceTypeIdentifier());

        // Delete the resource including related entities
        if (resource != null) {
            // Remove resource from IDS brokers
            if (!brokers.isEmpty() && !brokers.get(0).toString().isEmpty()) {
                for (var broker : brokers) {
                    try {
                        dscOperator.workflows().broker()
                                .unregisterResources(broker, List.of(
                                        URI.create(resource.getLinks().getSelf().getHref())));
                    } catch (Exception e) {
                        log.error("Failed to delete resource at broker: {}", e.getMessage());
                    }
                }
            }
    
            dscOperator.workflows().resourceRemoval().deleteFullResource(resource);
        }
    }
    
    /**
     * Returns the catalog that represents a given device type.
     *
     * @param typeIdentifier the device type identifier.
     * @return the catalog, if it exists; null otherwise.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if an error occurs mapping the input or response.
     */
    private CatalogOutput findDeviceTypeCatalog(final String typeIdentifier)
            throws ApiInteractionUnsuccessfulException, JsonProcessingException {
        CatalogOutput result = null;
    
        final var catalogs = dscOperator.entities().catalogs().getAll();
        for (var catalog : catalogs.getEmbedded().getEntries()) {
            if (typeIdentifier.equals(catalog.getTitle())) {
                result = catalog;
                break;
            }
        }
        
        return result;
    }
    
    /**
     * Returns the resource that represents a given device instance.
     *
     * @param instanceId the device instance ID.
     * @param typeIdentifier the device type identifier.
     * @return the resource, if it exists; null otherwise.
     * @throws EmptyFieldException if the catalog ID cannot be read.
     * @throws ApiInteractionUnsuccessfulException if the DSC returns a non 2xx status code.
     * @throws JsonProcessingException if an error occurs mapping the input or response.
     */
    private OfferedResourceOutput findDeviceInstanceResource(final String instanceId,
                                                             final String typeIdentifier)
            throws EmptyFieldException, ApiInteractionUnsuccessfulException,
            JsonProcessingException {
        OfferedResourceOutput result = null;

        // Find catalog for device type
        final var catalog = findDeviceTypeCatalog(typeIdentifier);
        if (catalog == null) {
            throw new IllegalStateException(format("No device type catalog found for type"
                    + " identifier %s. Cannot find device instance resource with ID %s.",
                    typeIdentifier, instanceId));
        }
        
        // Get all resources in catalog
        final var catalogId = UuidUtils.getUuidFromOutput(catalog);
        final var resources = dscOperator.entities()
                .catalogs()
                .offers()
                .getAll(catalogId);
        
        // Find resource representing the device instance
        for (var resource : resources.getEmbedded().getEntries()) {
            if (instanceId.equals(resource.getTitle())) {
                result = resource;
                break;
            }
        }
        
        return result;
    }
    
    /**
     * Updates the connector at IDS brokers. Registers all resources as connector update may
     * remove previously registered resources
     */
    private void updateBrokerRegistration() {
        if (!brokers.isEmpty() && !brokers.get(0).toString().isEmpty()) {
            for (var broker : brokers) {
                try {
                    dscOperator.workflows().broker().registerAllResources(broker);
                } catch (Exception e) {
                    log.error("Failed to update resources at broker: {}", e.getMessage());
                }
            }
        }
    }
    
    /**
     * Updates the broker registration of a specific resource.
     *
     * @param resourceUri URI of the resource to update.
     */
    private void updateBrokerRegistration(final URI resourceUri) {
        if (!brokers.isEmpty() && !brokers.get(0).toString().isEmpty()) {
            for (var broker : brokers) {
                try {
                    dscOperator.workflows().broker()
                            .registerResources(broker, List.of(resourceUri));
                } catch (Exception e) {
                    log.error("Failed to update resource at broker: {}", e.getMessage());
                }
            }
        }
    }

}
