/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.subscription.deserialization;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Jackson MixIn class for {@link org.siliconeconomy.iotbroker.model.device.DeviceInstance}.
 * Adds an annotated constructor for deserialization to the class.
 *
 * @author Ronja Quensel
 */
public class DeviceInstanceMixIn {
    
    /**
     * Constructor for {@link org.siliconeconomy.iotbroker.model.device.DeviceInstance} with
     * Jackson annotations.
     *
     * @param id the id.
     * @param source the source.
     * @param tenant the tenant.
     * @param deviceTypeIdentifier the type identifier.
     * @param registrationTime when the instance was registered.
     * @param lastSeen when the instance last sent updates.
     * @param enabled whether the instance is enabled.
     * @param description the description.
     * @param hardwareRevision the hardware revision.
     * @param firmwareVersion the software version.
     */
    @SuppressWarnings("squid:S107")
    public DeviceInstanceMixIn(@JsonProperty("id") String id,
                               @JsonProperty("source") String source,
                               @JsonProperty("tenant") String tenant,
                               @JsonProperty("deviceTypeIdentifier") String deviceTypeIdentifier,
                               @JsonProperty("registrationTime") Instant registrationTime,
                               @JsonProperty("lastSeen") Instant lastSeen,
                               @JsonProperty("enabled") boolean enabled,
                               @JsonProperty("description") String description,
                               @JsonProperty("hardwareRevision") String hardwareRevision,
                               @JsonProperty("firmwareVersion") String firmwareVersion) { }
    
}
