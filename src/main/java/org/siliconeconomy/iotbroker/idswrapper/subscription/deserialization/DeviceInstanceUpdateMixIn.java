/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.subscription.deserialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;

/**
 * Jackson MixIn class for {@link DeviceInstanceUpdate}. Adds an annotated constructor for
 * deserialization to the class.
 *
 * @author Ronja Quensel
 */
public class DeviceInstanceUpdateMixIn {
    
    /**
     * Constructor for {@link DeviceInstanceUpdate} with Jackson annotations.
     *
     * @param newState the new instance state.
     * @param oldState the old instance state.
     * @param type the type of update.
     */
    public DeviceInstanceUpdateMixIn(@JsonProperty("newState") DeviceInstance newState,
                                     @JsonProperty("oldState") DeviceInstance oldState,
                                     @JsonProperty("type") DeviceInstanceUpdate.Type type) { }
    
}
