/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.subscription.deserialization;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Jackson MixIn class for {@link org.siliconeconomy.iotbroker.model.device.DeviceType}.
 * Adds an annotated constructor for deserialization to the class.
 *
 * @author Ronja Quensel
 */
public class DeviceTypeMixIn {
    
    /**
     * Constructor for {@link org.siliconeconomy.iotbroker.model.device.DeviceType} with Jackson
     * annotations.
     *
     * @param id the id.
     * @param source the source.
     * @param identifier the type identifier.
     * @param providedBy providers of the type.
     * @param description the description.
     * @param enabled whether the type is enabled.
     * @param autoRegisterDeviceInstances whether new instances are automatically registered.
     * @param autoEnableDeviceInstances whether new instances are automatically enabled.
     */
    @SuppressWarnings("squid:S107")
    public DeviceTypeMixIn(@JsonProperty("id") String id, @JsonProperty("source") String source,
                           @JsonProperty("identifier") String identifier,
                           @JsonProperty("providedBy") Set<String> providedBy,
                           @JsonProperty("description") String description,
                           @JsonProperty("enabled") boolean enabled,
                           @JsonProperty("autoRegisterDeviceInstances")
                                       boolean autoRegisterDeviceInstances,
                           @JsonProperty("autoEnableDeviceInstances")
                                      boolean autoEnableDeviceInstances) { }
    
}
