/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.subscription.deserialization;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

/**
 * Jackson MixIn class for {@link DeviceTypeUpdate}. Adds an annotated constructor for
 * deserialization to the class.
 *
 * @author Ronja Quensel
 */
public class DeviceTypeUpdateMixIn {
    
    /**
     * Constructor for {@link DeviceTypeUpdate} with Jackson annotations.
     *
     * @param newState the new type state.
     * @param oldState the old type state.
     * @param type the type of update.
     */
    @JsonCreator
    public DeviceTypeUpdateMixIn(@JsonProperty("newState") DeviceType newState,
                                 @JsonProperty("oldState") DeviceType oldState,
                                 @JsonProperty("type") DeviceTypeUpdate.Type type) { }
    
}
