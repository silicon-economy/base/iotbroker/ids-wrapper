/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.utils;

import static java.lang.String.format;

import java.util.Map;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Provides several utility methods for recurring tasks.
 *
 * @author Malte Hellmeier, Ronja Quensel
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Utils {

    /**
     * Builds a full URL based on the host address, endpoint and parameters.
     * The query path parameters have to be included in the endpoint parameter since the
     * respective keys will replace all substrings in the form of "{key}" with their values.
     *
     * @param base            the host address.
     * @param endpoint        the endpoint.
     * @param pathParameters  all the path parameters.
     * @param queryParameters all the query parameters.
     * @return the built URL.
     */
    public static String buildUrl(final String base, final String endpoint,
                                  final Map<String, String> pathParameters,
                                  final Map<String, String> queryParameters) {
        var url = UriComponentsBuilder.fromHttpUrl(base)
                .path(endpoint)
                .build().toString();

        for (var entry : pathParameters.entrySet()) {
            url = url.replaceFirst("\\{" + entry.getKey() + "}", entry.getValue());
        }

        if (queryParameters.size() > 0) {
            final var queryBuilder = new StringBuilder("?");
            for (var entry : queryParameters.entrySet()) {
                queryBuilder.append(entry.getKey())
                        .append("=")
                        .append(entry.getValue())
                        .append("&");
            }
            queryBuilder.deleteCharAt(queryBuilder.length() - 1);
            url += queryBuilder.toString();
        }

        return url;
    }
    
    /**
     * Reads the device type or instance identifier from an AMQP routing key. Routing keys have
     * the format deviceType/deviceInstance.[identifier].update.
     *
     * @param routingKey the routing key.
     * @return the identifier.
     * @throws IllegalArgumentException if the routing key does not have the expected format
     */
    public static String identifierFromRoutingKey(final String routingKey) {
        try {
            return routingKey.split("\\.")[1];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException(format("Invalid routing key format: %s",
                    routingKey));
        }
       
    }

}
