/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.config;

import java.util.Objects;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for AmqpConfiguration
 *
 * @author Ronja Quensel
 */
@SpringBootTest(classes = {AmqpConfiguration.class})
class AmqpConfigurationTest {
    
    @Autowired
    private AmqpConfiguration configuration;
    
    @MockBean
    private ObjectMapper objectMapper;
    
    @MockBean
    private ConnectionFactory connectionFactory;
    
    @Test
    void messageConverter_returnConfiguredMessageConverter() {
        // Act
        var converter = configuration.messageConverter();
        
        // Assert
        assertThat(converter).isNotNull();
        assertThat(converter.getTypePrecedence())
                .isEqualTo(AmqpConfiguration.CONVERTER_TYPE_PRECEDENCE);
    }
    
    @Test
    void rabbitListenerContainerFactory_returnConfiguredContainerFactory() {
        // Act
        var factory = configuration.rabbitListenerContainerFactory(connectionFactory);
        
        // Assert
        assertThat(factory).isNotNull();
        assertThat(checkFieldValue(factory, "batchSize",
                AmqpConfiguration.FACTORY_BATCH_SIZE)).isTrue();
        assertThat(checkFieldValue(factory, "receiveTimeout",
                AmqpConfiguration.FACTORY_RECEIVE_TIMEOUT)).isTrue();
        assertThat(checkFieldValue(factory, "consumerBatchEnabled",
                AmqpConfiguration.FACTORY_CONSUMER_BATCH_ENABLED)).isTrue();
        assertThat(checkFieldValue(factory, "batchListener",
                AmqpConfiguration.FACTORY_BATCH_LISTENER)).isTrue();
        assertThat(checkFieldValue(factory, "deBatchingEnabled",
                AmqpConfiguration.FACTORY_DEBATCHING_ENABLED)).isTrue();
        assertThat(checkFieldValue(factory, "messageConverter",
                configuration.messageConverter())).isTrue();
        assertThat(checkFieldValue(factory, "connectionFactory", connectionFactory)).isTrue();
    }
    
    private boolean checkFieldValue(final SimpleRabbitListenerContainerFactory factory,
                                    final String fieldName, final Object expectedValue) {
        var fieldValue = ReflectionTestUtils.getField(factory, fieldName);
        return Objects.equals(fieldValue, expectedValue);
    }
    
}
