/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.config;

import java.lang.reflect.Method;

import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.iotbroker.idswrapper.service.DeviceAlignmentService;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

/**
 * Tests the DeviceAlignmentConfiguration.
 *
 * @author Ronja Quensel
 */
class DeviceAlignmentConfigurationTest {
    
    private DeviceAlignmentConfiguration configuration;
    private DeviceAlignmentService service;
    
    @BeforeEach
    void init() {
        service = mock(DeviceAlignmentService.class);
        configuration = new DeviceAlignmentConfiguration(service);
    }
    
    @Test
    @SneakyThrows
    void alignDevices_startAlignment() {
        // Arrange
        var method = getAccessibleMethod();
    
        doNothing().when(service).alignDevices();
        
        // Act & Assert
        assertDoesNotThrow(() -> method.invoke(configuration));
    }
    
    @Test
    @SneakyThrows
    void alignDevices_errorInAlignment_doNotThrow() {
        // Arrange
        var method = getAccessibleMethod();
        
        doThrow(ApiInteractionUnsuccessfulException.class).when(service).alignDevices();
    
        // Act & Assert
        assertDoesNotThrow(() -> method.invoke(configuration));
    }
    
    @SneakyThrows
    private Method getAccessibleMethod() {
        var method = configuration.getClass().getDeclaredMethod("alignDevices");
        method.setAccessible(true);
        return method;
    }
    
}
