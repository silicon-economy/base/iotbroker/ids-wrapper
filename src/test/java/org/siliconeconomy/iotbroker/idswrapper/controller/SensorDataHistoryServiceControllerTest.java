/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * SensorDataHistoryServiceControllerTest
 *
 * @author Florian Zimmer
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {SensorDataHistoryServiceController.class})
class SensorDataHistoryServiceControllerTest {

    /** Class under test */
    @Autowired
    private SensorDataHistoryServiceController sensorDataHistoryServiceController;

    @Value("${broker.uri}")
    private String brokerUri;

    @Mock
    private RestTemplate restTemplate;

    @Captor
    private ArgumentCaptor<String> urlCaptor;

    @BeforeEach
    void init() {
        ReflectionTestUtils.setField(sensorDataHistoryServiceController, "restTemplate",
                restTemplate);
    }

    @Test
    void getHistoryDataBySourceAndTenant_successfulRequest_shouldRespond() {
        // Arrange
        var source = "source";
        var tenant = "tenant";
        var startTime = "startTime";
        var endTime = "endTime";
        var offset = 0;
        var limit = 0;
        var sort = "sort";

        var expectedResponse = new ResponseEntity<>("responseBody", HttpStatus.OK);

        when(restTemplate.exchange(urlCaptor.capture(), any(), any(), eq(String.class)))
                .thenReturn(expectedResponse);

        // Act
        var result = sensorDataHistoryServiceController.getDataBySourceAndTenant(source, tenant,
                startTime, endTime, offset, limit, sort);

        // Assert
        var urlValue = urlCaptor.getValue();

        assertThat(urlValue)
                .contains(brokerUri)
                .contains("messages/sources/" + source + "/tenants/" + tenant)
                .contains("startTime=" + startTime)
                .contains("endTime=" + endTime)
                .contains("offset=" + offset)
                .contains("limit=" + limit)
                .contains("sort=" + sort);

        assertThat(result.getBody()).isEqualTo(expectedResponse.getBody());
        assertThat(result.getStatusCode()).isEqualTo(expectedResponse.getStatusCode());
    }
    
    @Test
    void getHistoryDataBySourceAndTenant_onlyOneParameterSet_shouldRespond() {
        // Arrange
        var source = "source";
        var tenant = "tenant";
        var startTime = "startTime";
        
        var expectedResponse = new ResponseEntity<>("responseBody", HttpStatus.OK);
        
        when(restTemplate.exchange(urlCaptor.capture(), any(), any(), eq(String.class)))
                .thenReturn(expectedResponse);
        
        // Act
        var result = sensorDataHistoryServiceController.getDataBySourceAndTenant(source, tenant,
                startTime, null, null, null, null);
        
        // Assert
        var urlValue = urlCaptor.getValue();
        
        assertThat(urlValue)
                .contains(brokerUri)
                .contains("messages/sources/" + source + "/tenants/" + tenant)
                .contains("startTime=" + startTime);
        
        assertThat(result.getBody()).isEqualTo(expectedResponse.getBody());
        assertThat(result.getStatusCode()).isEqualTo(expectedResponse.getStatusCode());
    }

    @Test
    void getHistoryDataBySourceAndTenant_unsuccessfulRequest_shouldThrowException() {
        // Arrange
        var source = "source";
        var tenant = "tenant";
        var startTime = "startTime";
        var endTime = "endTime";
        var offset = 0;
        var limit = 0;
        var sort = "sort";

        var expectedResponse = new ResponseEntity<>("Request to sensor data history service"
                + " could not be sent.",
                HttpStatus.INTERNAL_SERVER_ERROR);

        when(restTemplate.exchange(urlCaptor.capture(), any(), any(), eq(String.class)))
                .thenThrow(RestClientException.class);

        // Act
        var result = sensorDataHistoryServiceController.getDataBySourceAndTenant(source, tenant,
                startTime, endTime, offset, limit, sort);

        // Assert
        var urlValue = urlCaptor.getValue();

        assertThat(urlValue)
                .contains(brokerUri)
                .contains("messages/sources/" + source + "/tenants/" + tenant)
                .contains("startTime=" + startTime)
                .contains("endTime=" + endTime)
                .contains("offset=" + offset)
                .contains("limit=" + limit)
                .contains("sort=" + sort);

        assertThat(result.getBody()).isEqualTo(expectedResponse.getBody());
        assertThat(result.getStatusCode()).isEqualTo(expectedResponse.getStatusCode());
    }

    @Test
    void getLatestDataBySourceAndTenant_successfulRequest_shouldRespond() {
        // Arrange
        var source = "source";
        var tenant = "tenant";

        var expectedResponse = new ResponseEntity<>("responseBody", HttpStatus.OK);

        when(restTemplate.exchange(urlCaptor.capture(), any(), any(), eq(String.class)))
                .thenReturn(expectedResponse);

        // Act
        var result = sensorDataHistoryServiceController.getDataBySourceAndTenant(source,
                tenant, null, null, null, null, null);

        // Assert
        var urlValue = urlCaptor.getValue();

        assertThat(urlValue)
                .contains(brokerUri)
                .contains("messages/sources/" + source + "/tenants/" + tenant + "/latest");

        assertThat(result.getBody()).isEqualTo(expectedResponse.getBody());
        assertThat(result.getStatusCode()).isEqualTo(expectedResponse.getStatusCode());
    }

    @Test
    void getLatestDataBySourceAndTenant_unsuccessfulRequest_shouldThrowException() {
        // Arrange
        var source = "source";
        var tenant = "tenant";

        var expectedResponse = new ResponseEntity<>("Request to sensor data history service"
                + " could not be sent.",
                HttpStatus.INTERNAL_SERVER_ERROR);

        when(restTemplate.exchange(urlCaptor.capture(), any(), any(), eq(String.class)))
                .thenThrow(RestClientException.class);

        // Act
        var result = sensorDataHistoryServiceController.getDataBySourceAndTenant(source,
                tenant, null, null, null, null, null);

        // Assert
        var urlValue = urlCaptor.getValue();

        assertThat(urlValue)
                .contains(brokerUri)
                .contains("messages/sources/" + source + "/tenants/" + tenant + "/latest");

        assertThat(result.getBody()).isEqualTo(expectedResponse.getBody());
        assertThat(result.getStatusCode()).isEqualTo(expectedResponse.getStatusCode());
    }
}
