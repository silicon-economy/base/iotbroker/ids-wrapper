/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.service;

import java.net.URI;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.CatalogApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.CatalogOfferedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ContractApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EntityApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceRepresentationsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationArtifactsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ResourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.BrokerWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceCreationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceRemovalWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.WorkflowApi;
import org.siliconeconomy.idsintegrationtoolbox.model.RestRequest;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.CatalogEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.CatalogLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.CatalogMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.CatalogOfferedResourceRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestBuilder;
import org.siliconeconomy.idsintegrationtoolbox.utils.RestRequestHandler;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.iotbroker.idswrapper.config.MappingConfiguration;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

/**
 * DeviceAlignmentServiceTest
 *
 * @author Haydar Qarawlus
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {DeviceAlignmentService.class, MappingConfiguration.class})
class DeviceAlignmentServiceTest {
    
    private final String EXAMPLE_URI = "http://localhost:8080/api/example/06dc1644-f58d-4562-836f" +
            "-197c0a65c0d6";

    @Value("${device-registry.url}")
    private String brokerUri;

    @Value("${device-registry.device-instances-endpoint}")
    private String deviceInstancesEndpoint;

    @Value("${device-registry.device-types-endpoint}")
    private String deviceTypesEndpoint;
    
    @Value("${device-registry.page-size:100}")
    private int registryPageSize;
    
    @Value("#{'${ids.metadata-broker.urls}'.split(';')}")
    private List<URI> brokers;

    @MockBean
    private DeviceMappingService mappingService;

    @MockBean
    private ResourceCreationService creationService;

    @MockBean
    private EntityApi entityApi;

    @MockBean
    private CatalogApi catalogApi;

    @MockBean
    private CatalogOfferedResourcesApi catalogOfferedResourcesApi;

    @MockBean
    private OfferedResourceApi offeredResourceApi;

    @MockBean
    private ContractApi contractApi;

    @MockBean
    private ResourceApi resourceApi;

    @MockBean
    private WorkflowApi workflowApi;

    @MockBean
    private RepresentationApi representationApi;

    @MockBean
    private OfferedResourceRepresentationsApi offeredResourceRepresentationsApi;

    @MockBean
    private RepresentationArtifactsApi representationArtifactsApi;

    @MockBean
    private ResourceCreationWorkflow resourceCreationWorkflow;

    @MockBean
    private ResourceRemovalWorkflow resourceRemovalWorkflow;

    @MockBean
    private ArtifactApi artifactApi;

    @MockBean
    private BrokerWorkflow brokerWorkflow;

    @MockBean
    private DscOperator dscOperator;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private RestRequestHandler requestHandler;
    
    @MockBean
    private DeviceFilter deviceFilter;

    @Autowired
    private DeviceAlignmentService deviceAlignmentService;

    @BeforeEach
    @SneakyThrows
    void setup() {
        when(dscOperator.entities()).thenReturn(entityApi);
        when(entityApi.catalogs()).thenReturn(catalogApi);

        when(entityApi.resources()).thenReturn(resourceApi);
        when(resourceApi.offers()).thenReturn(offeredResourceApi);
        when(offeredResourceApi.representations())
                .thenReturn(offeredResourceRepresentationsApi);
        when(representationApi.artifacts()).thenReturn(representationArtifactsApi);
        when(entityApi.artifacts()).thenReturn(artifactApi);

        when(dscOperator.workflows()).thenReturn(workflowApi);
        when(workflowApi.resourceCreation()).thenReturn(resourceCreationWorkflow);
        when(workflowApi.resourceRemoval()).thenReturn(resourceRemovalWorkflow);
        when(workflowApi.broker()).thenReturn(brokerWorkflow);

        when(entityApi.representations()).thenReturn(representationApi);
        when(entityApi.artifacts()).thenReturn(artifactApi);
        when(entityApi.contracts()).thenReturn(contractApi);
        when(catalogApi.offers()).thenReturn(catalogOfferedResourcesApi);

        when(objectMapper.readValue(anyString(), any(CollectionType.class)))
                .thenReturn(getDeviceTypes())
                .thenReturn(getDeviceInstances());

        when(catalogOfferedResourcesApi.getAll(any()))
                .thenReturn(getCatalogOfferedResourceRelationOutput(getOfferedResourceOutput()));

        when(offeredResourceApi.getOne(any())).thenReturn(getOfferedResourceOutput());

        when(catalogApi.create(any())).thenReturn(getCatalog(UUID.randomUUID().toString(),
                "652e9dd6-881d-11ec-a8a3-0242ac120001", "description"));
        
        when(deviceFilter.filterTypes(any())).thenAnswer(i -> i.getArgument(0));
        when(deviceFilter.filterInstances(any())).thenAnswer(i -> i.getArgument(0));
    }
    
    @Test
    void constructor_supplyNullRestRequestHandler_throwException() {
        assertThatThrownBy(() -> new DeviceAlignmentService(null, dscOperator, mappingService, creationService, objectMapper, deviceFilter))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void constructor_supplyNullDscOperator_throwException() {
        assertThatThrownBy(() -> new DeviceAlignmentService(requestHandler, null, mappingService, creationService, objectMapper, deviceFilter))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void constructor_supplyNullDeviceMappingService_throwException() {
        assertThatThrownBy(() -> new DeviceAlignmentService(requestHandler, dscOperator, null, creationService, objectMapper, deviceFilter))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void constructor_supplyNullResourceCreationService_throwException() {
        assertThatThrownBy(() -> new DeviceAlignmentService(requestHandler, dscOperator, mappingService, null, objectMapper, deviceFilter))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void constructor_supplyNullObjectMapper_throwException() {
        assertThatThrownBy(() -> new DeviceAlignmentService(requestHandler, dscOperator, mappingService, creationService, null, deviceFilter))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void constructor_supplyNullDeviceFilter_throwException() {
        assertThatThrownBy(() -> new DeviceAlignmentService(requestHandler, dscOperator, mappingService, creationService, objectMapper, null))
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    @SneakyThrows
    void alignDevices_validInputs_alignmentComplete() {
        // Arrange
        when(requestHandler.sendRequest(getSampleTypesRestRequest()))
                .thenReturn("response");
        when(requestHandler.sendRequest(getSampleInstancesRestRequest()))
                .thenReturn("response");

        when(catalogApi.getAll()).thenReturn(getCatalogMultiOutput());
        when(catalogOfferedResourcesApi.getAll(any()))
                .thenReturn(getCatalogOfferedResourceRelationOutput(getOfferedResourceOutput()));
        
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());

            // Act
            assertDoesNotThrow(() -> deviceAlignmentService.alignDevices());
        }
    
        // Assert
        verify(requestHandler, times(2)).sendRequest(any());
        verify(catalogApi, times(1)).getAll();
        verify(creationService, times(3)).createResource(any(), any());
        verify(resourceRemovalWorkflow, times(2))
                .deleteResourceAndCatalog(any(OfferedResourceOutput.class));
    
        verify(brokerWorkflow).registerAllResources(brokers.get(0));
        verify(brokerWorkflow).registerAllResources(brokers.get(1));
    }
    
    @Test
    @SneakyThrows
    void alignDevices_validInputs_noBrokersDefined_alignmentComplete() {
        // Arrange
        emptyBrokerList();
    
        when(requestHandler.sendRequest(getSampleTypesRestRequest()))
                .thenReturn("response");
        when(requestHandler.sendRequest(getSampleInstancesRestRequest()))
                .thenReturn("response");
        
        when(catalogApi.getAll()).thenReturn(getCatalogMultiOutput());
        when(catalogOfferedResourcesApi.getAll(any()))
                .thenReturn(getCatalogOfferedResourceRelationOutput(getOfferedResourceOutput()));
        
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());
            
            // Act
            assertDoesNotThrow(() -> deviceAlignmentService.alignDevices());
        }
        
        // Assert
        verify(requestHandler, times(2)).sendRequest(any());
        verify(catalogApi, times(1)).getAll();
        verify(creationService, times(3)).createResource(any(), any());
        verify(resourceRemovalWorkflow, times(2))
                .deleteResourceAndCatalog(any(OfferedResourceOutput.class));
        
        verifyNoInteractions(brokerWorkflow);
        
        // Clean-up
        restoreBrokerList();
    }
    
    @Test
    @SneakyThrows
    void alignDevices_catalogMatchesIotBrokerDevices_noAlignmentRequired() {
        // Arrange
        when(requestHandler.sendRequest(getSampleTypesRestRequest()))
                .thenReturn("response");
        when(requestHandler.sendRequest(getSampleInstancesRestRequest()))
                .thenReturn("response");
        
        when(catalogApi.getAll()).thenReturn(getCatalogMultiOutputOneElement());
        when(catalogOfferedResourcesApi.getAll(any()))
                .thenReturn(getCatalogOfferedResourceRelationOutput(getOfferedResourceOutputMatchingDeviceInstance()));
        
        when(objectMapper.readValue(anyString(), any(CollectionType.class))).
                thenReturn(List.of(getDeviceType("652e9dd6-881d-11ec-a8a3-0242ac120001", "lowcosttracker_v1")))
                .thenReturn(List.of(getDeviceInstance("652e9dd6-881d-11ec-a8a3-0242ac120001", "lowcosttracker1", "Tracker-41")));
        
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());
            
            // Act
            assertDoesNotThrow(() -> deviceAlignmentService.alignDevices());
        }
        
        // Assert
        verify(requestHandler, times(2)).sendRequest(any());
        verify(catalogApi, times(1)).getAll();
    
        verifyNoInteractions(creationService);
        verifyNoInteractions(resourceRemovalWorkflow);
        
        verify(brokerWorkflow).registerAllResources(brokers.get(0));
        verify(brokerWorkflow).registerAllResources(brokers.get(1));
    }

    @Test
    @SneakyThrows
    void alignDevices_iotBrokerOffline_typeRequestFails_alignmentFails() {
        // Arrange
        when(requestHandler.sendRequest(getSampleTypesRestRequest()))
                .thenThrow(new ApiInteractionUnsuccessfulException("Unable to communicate with Broker"));
        when(requestHandler.sendRequest(getSampleInstancesRestRequest()))
                .thenReturn("response");

        when(catalogApi.getAll()).thenReturn(getCatalogMultiOutput());

        when(catalogOfferedResourcesApi.getAll(any()))
                .thenReturn(getCatalogOfferedResourceRelationOutput(getOfferedResourceOutput()));

        // Act
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());

            // Act
            assertThrows(ApiInteractionUnsuccessfulException.class,
                    () -> deviceAlignmentService.alignDevices());
        }
    
        verifyNoInteractions(creationService);
        verifyNoInteractions(resourceRemovalWorkflow);
    }

    @Test
    @SneakyThrows
    void alignDevices_iotBrokerOffline_instancesRequestFails_alignmentFails() {
        // Arrange
        when(requestHandler.sendRequest(getSampleInstancesRestRequest()))
                .thenThrow(new ApiInteractionUnsuccessfulException("Unable to communicate with Broker"));
        when(requestHandler.sendRequest(getSampleTypesRestRequest()))
                .thenReturn("response");

        when(catalogApi.getAll()).thenReturn(getCatalogMultiOutput());

        when(catalogOfferedResourcesApi.getAll(any()))
                .thenReturn(getCatalogOfferedResourceRelationOutput(getOfferedResourceOutput()));

        // Act
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());

            // Act
            assertThrows(ApiInteractionUnsuccessfulException.class,
                    () -> deviceAlignmentService.alignDevices());
        }
    
        verifyNoInteractions(creationService);
        verifyNoInteractions(resourceRemovalWorkflow);
    }
    
    private OfferedResourceOutput getOfferedResourceOutput() {
        return getOfferedResourceOutput("652e9dd6-881d-11ec-a8a3-0242ac120008");
    }
    
    private OfferedResourceOutput getOfferedResourceOutputMatchingDeviceInstance() {
        return getOfferedResourceOutput("652e9dd6-881d-11ec-a8a3-0242ac120001");
    }
    
    private OfferedResourceOutput getOfferedResourceOutput(final String title) {
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", EXAMPLE_URI);
    
        final var offeredResourceLinks = new OfferedResourceLinks();
        ReflectionTestUtils.setField(offeredResourceLinks, "self", link);
        
        final var offeredResourceSingleOutput = new OfferedResourceOutput();
        ReflectionTestUtils.setField(offeredResourceSingleOutput, "title", title);
        ReflectionTestUtils.setField(offeredResourceSingleOutput, "links", offeredResourceLinks);
        
        return offeredResourceSingleOutput;
    }

    private CatalogOfferedResourceRelationOutput getCatalogOfferedResourceRelationOutput(final OfferedResourceOutput resource) {
        // Mock catalog and its relation api operator in the deleteOrUnlink method.
        final var catalogOfferedResourceRelationOutput =
                new CatalogOfferedResourceRelationOutput();
        final var offeredResourceEmbedded = new OfferedResourceEmbedded();
        
        ReflectionTestUtils.setField(offeredResourceEmbedded, "entries", List.of(resource));
        ReflectionTestUtils.setField(catalogOfferedResourceRelationOutput,
                "embedded", offeredResourceEmbedded);

        return catalogOfferedResourceRelationOutput;
    }

    private CatalogOutput getCatalog(String Uuid, String identifier, String description) {
        var catalogOutput = new CatalogOutput();
        var catalogLinks = new CatalogLinks();
        var catalogSelfLink = new Link();

        ReflectionTestUtils.setField(catalogSelfLink, "href",
                "https://example.com/catalog/" + Uuid);
        ReflectionTestUtils.setField(catalogLinks, "self", catalogSelfLink);
        ReflectionTestUtils.setField(catalogOutput, "links", catalogLinks);

        catalogOutput.setTitle(identifier);
        catalogOutput.setDescription(description);

        return catalogOutput;
    }
    
    private CatalogMultiOutput getCatalogMultiOutput() {
        var catalogMultiOutput = new CatalogMultiOutput();
        var catalogEmbedded = new CatalogEmbedded();
        var devicesList = Arrays.asList("lowcosttracker_v4", "lowcosttracker_v5");

        var catalogList = new ArrayList<CatalogOutput>();
        for (var deviceIdentifier : devicesList) {
            catalogList.add(getCatalog(UUID.randomUUID().toString(), deviceIdentifier,
                    "description"));
        }

        ReflectionTestUtils.setField(catalogEmbedded, "entries", catalogList);
        ReflectionTestUtils.setField(catalogMultiOutput, "embedded", catalogEmbedded);

        return catalogMultiOutput;
    }
    
    private CatalogMultiOutput getCatalogMultiOutputOneElement() {
        var catalogMultiOutput = new CatalogMultiOutput();
        var catalogEmbedded = new CatalogEmbedded();
        
        var catalogList = new ArrayList<CatalogOutput>();
        catalogList.add(getCatalog(UUID.randomUUID().toString(), "lowcosttracker_v1",
                "description"));
        
        ReflectionTestUtils.setField(catalogEmbedded, "entries", catalogList);
        ReflectionTestUtils.setField(catalogMultiOutput, "embedded", catalogEmbedded);
        
        return catalogMultiOutput;
    }

    private DeviceType getDeviceType(String id, String identifier){
        return new DeviceType(
                id,
                "lowcosttracker",
                identifier,
                Set.of("lowcosttracker-adapter"),
                "Devices for locating and tracking goods.",
                true,
                true,
                true
        );
    }

    List<DeviceType> getDeviceTypes() {
        return new ArrayList<DeviceType>() {{
                add(getDeviceType("652e9dd6-881d-11ec-a8a3-0242ac120001", "lowcosttracker_v1"));
                add(getDeviceType("652e9dd6-881d-11ec-a8a3-0242ac120002", "lowcosttracker_v2"));
                add(getDeviceType("652e9dd6-881d-11ec-a8a3-0242ac120003", "lowcosttracker_v3"));
        }};
    }

    DeviceInstance getDeviceInstance(String id, String source, String tenant) {
        return new DeviceInstance(
                id,
                source,
                tenant,
                "lowcosttracker_v1",
                Instant.now(),
                Instant.now(),
                true,
                "Device for locating and tracking goods.",
                "v1.0.0",
                "v1.0.0"
        );
    }

    private List<DeviceInstance> getDeviceInstances() {
        return new ArrayList<DeviceInstance>() {{
                add(getDeviceInstance("652e9dd6-881d-11ec-a8a3-0242ac120001",
                        "lowcosttracker1", "Tracker-41"));
                add(getDeviceInstance("652e9dd6-881d-11ec-a8a3-0242ac120002",
                        "lowcosttracker2", "Tracker-42"));
                add(getDeviceInstance("652e9dd6-881d-11ec-a8a3-0242ac120003",
                        "lowcosttracker3", "Tracker-43"));
        }};
    }
    
    private RestRequest getSampleTypesRestRequest() {
        return new RestRequestBuilder(HttpMethod.GET, brokerUri, deviceTypesEndpoint)
                .queryParameter("pageSize", String.valueOf(registryPageSize))
                .queryParameter("pageIndex", "0")
                .build();
    }

    private RestRequest getSampleInstancesRestRequest() {
        return new RestRequestBuilder(HttpMethod.GET, brokerUri, deviceInstancesEndpoint)
                .queryParameter("pageSize", String.valueOf(registryPageSize))
                .queryParameter("pageIndex", "0")
                .build();
    }
    
    private void emptyBrokerList() {
        ReflectionTestUtils.setField(deviceAlignmentService, "brokers", new ArrayList<>());
    }
    
    private void restoreBrokerList() {
        ReflectionTestUtils.setField(deviceAlignmentService, "brokers", brokers);
    }

}
