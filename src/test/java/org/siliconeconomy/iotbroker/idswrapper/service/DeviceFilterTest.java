/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.siliconeconomy.iotbroker.model.sensordata.Location;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.siliconeconomy.iotbroker.model.sensordata.location.SimpleDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for DeviceFilter.
 *
 * @author Ronja Quensel
 */
@SpringBootTest(classes = {DeviceFilter.class},
        properties = {"device-filter.enabled=true", "device-filter.allowed-sources=testdevice"})
class DeviceFilterTest {
    
    private final String whitelistedSource = "testdevice";

    @Autowired
    private DeviceFilter deviceFilter;
    
    @Test
    void filterTypes_returnWhitelistedDeviceTypes() {
        // Arrange
        var allowedDevice = getDeviceType(whitelistedSource);
        var types = List.of(allowedDevice, getDeviceType("some-other-source"));
        
        // Act
        var filteredTypes = deviceFilter.filterTypes(types);
        
        // Assert
        assertThat(filteredTypes).hasSize(1).containsExactly(allowedDevice);
    }
    
    @Test
    void filterTypes_filterNotEnabled_returnAllDeviceTypes() {
        // Arrange
        disableDeviceFilter();
        var types = List.of(getDeviceType(whitelistedSource), getDeviceType("some-other-source"));
    
        // Act
        var filteredTypes = deviceFilter.filterTypes(types);
    
        // Assert
        assertThat(filteredTypes).hasSize(2).containsAll(types);
        
        // Clean-up
        enableDeviceFilter();
    }
    
    @Test
    void filterInstances_returnWhitelistedDeviceInstances() {
        // Arrange
        var allowedInstance = getDeviceInstance(whitelistedSource);
        var instances = List.of(allowedInstance, getDeviceInstance("some-other-source"));
    
        // Act
        var filteredInstances = deviceFilter.filterInstances(instances);
    
        // Assert
        assertThat(filteredInstances).hasSize(1).containsExactly(allowedInstance);
    }
    
    @Test
    void filterInstances_filterNotEnabled_returnAllDeviceInstances() {
        // Arrange
        disableDeviceFilter();
        var instances = List.of(getDeviceInstance(whitelistedSource), getDeviceInstance("some-other-source"));
    
        // Act
        var filteredTypes = deviceFilter.filterInstances(instances);
    
        // Assert
        assertThat(filteredTypes).hasSize(2).containsAll(instances);
    
        // Clean-up
        enableDeviceFilter();
    }
    
    @Test
    void isProcessingAllowed_sensorDataMessage_sourceWhitelisted_returnTrue() {
        // Arrange
        var message = getSensorDataMessage(whitelistedSource);
        
        // Act
        var result = deviceFilter.isProcessingAllowed(message);
        
        // Assert
        assertThat(result).isTrue();
    }
    
    @Test
    void isProcessingAllowed_sensorDataMessage_sourceNotWhitelisted_returnFalse() {
        // Arrange
        var message = getSensorDataMessage("some-other-source");
    
        // Act
        var result = deviceFilter.isProcessingAllowed(message);
    
        // Assert
        assertThat(result).isFalse();
    }
    
    @Test
    void isProcessingAllowed_sensorDataMessage_filterNotEnabled_returnTrue() {
        // Arrange
        disableDeviceFilter();
        var message = getSensorDataMessage("some-other-source");
        
        // Act
        var result = deviceFilter.isProcessingAllowed(message);
        
        // Assert
        assertThat(result).isTrue();
    
        // Clean-up
        enableDeviceFilter();
    }
    
    @Test
    void isProcessingAllowed_deviceTypeUpdate_sourcesWhitelisted_returnTrue() {
        // Arrange
        var update = getDeviceTypeUpdate(getDeviceType(whitelistedSource),
                getDeviceType(whitelistedSource));
    
        // Act
        var result = deviceFilter.isProcessingAllowed(update);
    
        // Assert
        assertThat(result).isTrue();
    }
    
    @Test
    void isProcessingAllowed_deviceTypeUpdate_sourcesNotWhitelisted_returnFalse() {
        // Arrange
        var update = getDeviceTypeUpdate(getDeviceType("some-other-source"),
                getDeviceType("some-other-source"));
    
        // Act
        var result = deviceFilter.isProcessingAllowed(update);
    
        // Assert
        assertThat(result).isFalse();
    }
    
    @Test
    void isProcessingAllowed_deviceTypeUpdate_oldSourceNotWhitelisted_returnFalse() {
        // Arrange
        var update = getDeviceTypeUpdate(getDeviceType("some-other-source"),
                getDeviceType(whitelistedSource));
    
        // Act
        var result = deviceFilter.isProcessingAllowed(update);
    
        // Assert
        assertThat(result).isFalse();
    }
    
    @Test
    void isProcessingAllowed_deviceTypeUpdate_newSourceNotWhitelisted_returnFalse() {
        // Arrange
        var update = getDeviceTypeUpdate(getDeviceType(whitelistedSource),
                getDeviceType("some-other-source"));
    
        // Act
        var result = deviceFilter.isProcessingAllowed(update);
    
        // Assert
        assertThat(result).isFalse();
    }
    
    @Test
    void isProcessingAllowed_deviceInstanceUpdate_sourcesWhitelisted_returnTrue() {
        // Arrange
        var update = getDeviceInstanceUpdate(getDeviceInstance(whitelistedSource),
                getDeviceInstance(whitelistedSource));
        
        // Act
        var result = deviceFilter.isProcessingAllowed(update);
        
        // Assert
        assertThat(result).isTrue();
    }
    
    @Test
    void isProcessingAllowed_deviceInstanceUpdate_sourcesNotWhitelisted_returnFalse() {
        // Arrange
        var update = getDeviceInstanceUpdate(getDeviceInstance("some-other-source"),
                getDeviceInstance("some-other-source"));
        
        // Act
        var result = deviceFilter.isProcessingAllowed(update);
        
        // Assert
        assertThat(result).isFalse();
    }
    
    @Test
    void isProcessingAllowed_deviceInstanceUpdate_oldSourceNotWhitelisted_returnFalse() {
        // Arrange
        var update = getDeviceInstanceUpdate(getDeviceInstance("some-other-source"),
                getDeviceInstance(whitelistedSource));
        
        // Act
        var result = deviceFilter.isProcessingAllowed(update);
        
        // Assert
        assertThat(result).isFalse();
    }
    
    @Test
    void isProcessingAllowed_deviceInstanceUpdate_newSourceNotWhitelisted_returnFalse() {
        // Arrange
        var update = getDeviceInstanceUpdate(getDeviceInstance(whitelistedSource),
                getDeviceInstance("some-other-source"));
        
        // Act
        var result = deviceFilter.isProcessingAllowed(update);
        
        // Assert
        assertThat(result).isFalse();
    }
    
    @Test
    @SneakyThrows
    void isProcessingAllowed_deviceInstanceUpdate_filterNotEnabled_returnTrue() {
        // Arrange
        disableDeviceFilter();
        var update = getDeviceInstanceUpdate(getDeviceInstance(whitelistedSource),
                getDeviceInstance("some-other-source"));
    
        // Act
        var result = deviceFilter.isProcessingAllowed(update);
    
        // Assert
        assertThat(result).isTrue();
        
        // Clean-up
        enableDeviceFilter();
    }
    
    private DeviceType getDeviceType(final String source) {
        return new DeviceType("id", source, "identifier", new HashSet<>(),
                "desc", true, true, true);
    }
    
    private DeviceInstance getDeviceInstance(final String source) {
        return new DeviceInstance("id", source, "tenant", "identifier",
                Instant.now(), Instant.now(), true, "desc", "1", "1");
    }
    
    private SensorDataMessage getSensorDataMessage(final String source) {
        var location = new Location<>("name", "desc", SimpleDetails.class,
                new SimpleDetails("details"));
        return new SensorDataMessage("id", "origin", source, "tenant",
                Instant.now(), location, new ArrayList<>());
    }
    
    private DeviceTypeUpdate getDeviceTypeUpdate(final DeviceType oldState,
                                                 final DeviceType newState) {
        return new DeviceTypeUpdate(newState, oldState, DeviceTypeUpdate.Type.CREATED);
    }
    
    private DeviceInstanceUpdate getDeviceInstanceUpdate(final DeviceInstance oldState,
                                                         final DeviceInstance newState) {
        return new DeviceInstanceUpdate(newState, oldState, DeviceInstanceUpdate.Type.CREATED);
    }
    
    @SneakyThrows
    private void disableDeviceFilter() {
        ReflectionTestUtils.setField(deviceFilter, "deviceFilterEnabled", false);
    }
    
    @SneakyThrows
    private void enableDeviceFilter() {
        ReflectionTestUtils.setField(deviceFilter, "deviceFilterEnabled", true);
    }
}
