/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.service;

import java.time.Instant;
import java.util.HashSet;

import de.fraunhofer.iais.eis.Permission;
import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PaymentMethod;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for DeviceMappingService.
 *
 * @author Ronja Quensel
 */
@SpringBootTest(classes = {DeviceMappingService.class})
class DeviceMappingServiceTest {
    
    @Autowired
    private DeviceMappingService mappingService;
    
    @Test
    void deviceTypeToCatalog_returnCatalogInput() {
        // Arrange
        var type = getDeviceType();
        
        // Act
        var catalog = mappingService.deviceTypeToCatalog(type);
        
        // Assert
        assertThat(catalog).isNotNull();
        assertThat(catalog.getTitle()).isEqualTo(type.getIdentifier());
        assertThat(catalog.getDescription()).isEqualTo(type.getDescription());
    }
    
    @Test
    void deviceInstanceToResource_returnResourceInput() {
        // Arrange
        var instance = getDeviceInstance();
    
        // Act
        var resource = mappingService.deviceInstanceToResource(instance);
    
        // Assert
        assertThat(resource).isNotNull();
        assertThat(resource.getTitle()).isEqualTo(instance.getId());
        assertThat(resource.getDescription()).isEqualTo(instance.getDescription());
        assertThat(resource.getKeywords())
                .containsExactly(instance.getSource(), instance.getTenant());
        assertThat(resource.getLanguage()).isEqualTo("en");
        assertThat(resource.getPaymentMethod()).isEqualTo(PaymentMethod.UNDEFINED);
    }
    
    @Test
    void deviceInstanceToRepresentation_returnRepresentationInput() {
        // Arrange
        var instance = getDeviceInstance();
        
        // Act
        var representation = mappingService.deviceInstanceToRepresentation(instance);
        
        // Assert
        assertThat(representation).isNotNull();
        assertThat(representation.getTitle()).isEqualTo(instance.getId());
        assertThat(representation.getMediaType()).isEqualTo("json");
    }
    
    @Test
    @SneakyThrows
    void deviceInstanceToArtifact_returnArtifactInput() {
        // Arrange
        var instance = getDeviceInstance();
        
        // Act
        var artifact = mappingService.deviceInstanceToArtifact(instance);
        
        // Assert
        assertThat(artifact).isNotNull();
        assertThat(artifact.getTitle()).isEqualTo(instance.getId());
        assertThat(artifact.getAccessUrl().toString())
                .contains(instance.getSource())
                .contains(instance.getTenant());
        
    }
    
    @Test
    @SneakyThrows
    void getProvideAccessRule_returnValidIdsRule() {
        // Act
        var rule = mappingService.getProvideAccessRule();
        
        // Assert
        assertThat(rule).isNotNull();
        assertThat(rule.getValue()).isNotNull().isNotEmpty().isNotBlank();

        var idsRule = new Serializer().deserialize(rule.getValue(), Permission.class);
        assertThat(idsRule).isNotNull();
    }
    
    private DeviceType getDeviceType() {
        return new DeviceType("id", "source", "identifier", new HashSet<>(),
                "description", true, true, true);

    }
    
    private DeviceInstance getDeviceInstance() {
        return new DeviceInstance("id", "source", "tenant", "typeIdentifier",
                Instant.now(), Instant.now(), true, "description", "1.0", "1.0");
    }
    
}
