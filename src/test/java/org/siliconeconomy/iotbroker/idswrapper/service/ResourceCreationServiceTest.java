/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.service;

import java.net.URI;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;

import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ContractApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EntityApi;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceCreationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.WorkflowApi;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ContractInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RepresentationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RuleInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ContractEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ContractLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.ContractMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ContractOutput;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for ResourceCreationService.
 *
 * @author Ronja Quensel
 */
@SpringBootTest(classes = {ResourceCreationService.class})
class ResourceCreationServiceTest {
    
    @Value("${device-instance.contract.title:IoT Broker Device Contract}")
    private String contractTitle;
    
    @Autowired
    private ResourceCreationService creationService;
    
    @MockBean
    private DeviceMappingService mappingService;
    
    @MockBean
    private DscOperator dscOperator;
    
    @MockBean
    private WorkflowApi workflowApi;
    
    @MockBean
    private ResourceCreationWorkflow resourceCreationWorkflow;
    
    @MockBean
    private EntityApi entityApi;
    
    @MockBean
    private ContractApi contractApi;
    
    @Captor
    private ArgumentCaptor<ContractInput> contractCaptor;
    
    @Captor
    private ArgumentCaptor<RuleInput> ruleCaptor;
    
    private URI contractUri;
    
    private URI catalogUri;
    
    private DeviceInstance deviceInstance;
    
    private OfferedResourceInput resourceInput;
    
    private RepresentationInput representationInput;
    
    private ArtifactInput artifactInput;
    
    @BeforeEach
    @SneakyThrows
    void init() {
        when(dscOperator.workflows()).thenReturn(workflowApi);
        when(workflowApi.resourceCreation()).thenReturn(resourceCreationWorkflow);
        
        when(dscOperator.entities()).thenReturn(entityApi);
        when(entityApi.contracts()).thenReturn(contractApi);
        
        resourceInput = new OfferedResourceInput();
        representationInput = new RepresentationInput();
        artifactInput = new ArtifactInput("title", "value", true);
        
        when(mappingService.deviceInstanceToResource(any())).thenReturn(resourceInput);
        when(mappingService.deviceInstanceToRepresentation(any()))
                .thenReturn(representationInput);
        when(mappingService.deviceInstanceToArtifact(any())).thenReturn(artifactInput);
    
        contractUri = URI.create("https://contract");
        catalogUri = URI.create("https://catalog");
        deviceInstance = new DeviceInstance("id", "source", "tenant", "typeIdentifier",
                Instant.now(), Instant.now(), true, "description", "1.0", "1.0");
    }
    
    @Test
    @SneakyThrows
    void createResource_contractAlreadyExists() {
        // Arrange
        when(contractApi.getAll()).thenReturn(getContractOutput(true));
        
        // Act
        creationService.createResource(catalogUri, deviceInstance);
        
        // Assert
        verify(resourceCreationWorkflow, times(1)).createResource(
                resourceInput, catalogUri, representationInput, artifactInput, contractUri);
    }
    
    @Test
    @SneakyThrows
    void createResource_contractDoesNotExist() {
        // Arrange
        final var ruleInput = new RuleInput("value");
        when(contractApi.getAll()).thenReturn(getContractOutput(false));
        when(mappingService.getProvideAccessRule()).thenReturn(ruleInput);
        
        // Act
        creationService.createResource(catalogUri, deviceInstance);
        
        // Assert
        verify(resourceCreationWorkflow, times(1))
                .createResource(eq(resourceInput), eq(catalogUri), eq(representationInput),
                        eq(artifactInput), contractCaptor.capture(), ruleCaptor.capture());
        
        var contractInput = contractCaptor.getValue();
        assertThat(contractInput.getTitle()).isEqualTo(contractTitle);
        assertThat(contractInput.getEnd()).isAfter(ZonedDateTime.now());
        
        assertThat(ruleCaptor.getValue()).isEqualTo(ruleInput);
    }
    
    private ContractMultiOutput getContractOutput(final boolean contractExists) {
        final var output = new ContractOutput();
        if (contractExists) {
            ReflectionTestUtils.setField(output, "title", contractTitle);
            final var link = new Link();
            ReflectionTestUtils.setField(link, "href", contractUri.toString());
            final var contractLinks = new ContractLinks();
            ReflectionTestUtils.setField(contractLinks, "self", link);
            ReflectionTestUtils.setField(output, "links", contractLinks);
        } else {
            ReflectionTestUtils.setField(output, "title", "another title");
        }
        
        final var embedded = new ContractEmbedded();
        ReflectionTestUtils.setField(embedded, "entries", List.of(output));
        
        final var multiOutput = new ContractMultiOutput();
        ReflectionTestUtils.setField(multiOutput, "embedded", embedded);
        
        return multiOutput;
    }
}
