/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.subscription;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EntityApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ResourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.ids.IdsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.ids.MessagesApi;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.OfferedResourceMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.iotbroker.idswrapper.config.MappingConfiguration;
import org.siliconeconomy.iotbroker.idswrapper.service.DeviceFilter;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

/**
 * Test for DeviceDataMessageListener.
 *
 * @author Ronja Quensel
 */
@SpringBootTest(classes = {DeviceDataMessageListener.class, MappingConfiguration.class})
class DeviceDataMessageListenerTest {
    
    @Value("${connector.url}")
    private String internalConnectorUrl;
    
    @Value("${connector.public-url}")
    private String publicConnectorUrl;

    @Autowired
    private DeviceDataMessageListener messageListener;
    
    @Autowired
    private @Qualifier("iotBrokerWrapperObjectMapper")
    ObjectMapper objectMapper;
    
    @MockBean
    private DscOperator dscOperator;
    
    @MockBean
    private EntityApi entityApi;
    
    @MockBean
    private ResourceApi resourceApi;
    
    @MockBean
    private OfferedResourceApi offeredResourceApi;
    
    @MockBean
    private IdsApi idsApi;
    
    @MockBean
    private MessagesApi messagesApi;
    
    @MockBean
    private DeviceFilter deviceFilter;
    
    @Mock
    private OfferedResourceMultiOutput resourceMultiOutput;
    
    @Mock
    private OfferedResourceEmbedded resourceEmbedded;
    
    @Mock
    private OfferedResourceOutput resource;

    @Mock
    private OfferedResourceLinks resourceLinks;
    
    @Mock
    private Link link;
    
    private URI href;
    
    private String source;
    
    private String tenant;
    
    @Mock
    private Channel channel;
    
    @BeforeEach
    @SneakyThrows
    void init() {
        href = URI.create("https://resource");
        source = "source";
        tenant = "tenant";
        
        when(dscOperator.entities()).thenReturn(entityApi);
        when(entityApi.resources()).thenReturn(resourceApi);
        when(resourceApi.offers()).thenReturn(offeredResourceApi);
        when(dscOperator.ids()).thenReturn(idsApi);
        when(idsApi.messages()).thenReturn(messagesApi);
        doNothing().when(messagesApi).notify(any());
        
        when(resource.getLinks()).thenReturn(resourceLinks);
        when(resourceLinks.getSelf()).thenReturn(link);
        when(link.getHref()).thenReturn(href.toString());
        
        when(deviceFilter.isProcessingAllowed(any(SensorDataMessage.class))).thenReturn(true);
    }
    
    @Test
    void constructor_supplyNullDscOperator_throwException() {
        assertThatThrownBy(() -> new DeviceDataMessageListener(null, objectMapper, deviceFilter))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void constructor_supplyNullObjectMapper_throwException() {
        assertThatThrownBy(() -> new DeviceDataMessageListener(dscOperator, null, deviceFilter))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void constructor_supplyNullDeviceFilter_throwException() {
        assertThatThrownBy(() -> new DeviceDataMessageListener(dscOperator, objectMapper, null))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void processDeviceDataUpdates_notAllowedByWhitelist_doNothing() {
        // Arrange
        when(deviceFilter.isProcessingAllowed(any(SensorDataMessage.class))).thenReturn(false);
    
        // Act
        messageListener.processDeviceDataUpdates(List.of(getMessage()), channel);
    
        // Assert
        verifyNoInteractions(dscOperator);
    }
    
    @Test
    @SneakyThrows
    void processDeviceDataUpdates_resourceExists_triggerIdsUpdates() {
        // Arrange
        when(offeredResourceApi.getAll()).thenReturn(resourceMultiOutput);
        when(resourceMultiOutput.getEmbedded()).thenReturn(resourceEmbedded);
        when(resourceEmbedded.getEntries()).thenReturn(List.of(resource));
        when(resource.getKeywords()).thenReturn(List.of(source, tenant));
        
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());
    
            // Act
            messageListener.processDeviceDataUpdates(List.of(getMessage()), channel);
    
            // Assert
            verify(messagesApi, times(1)).notify(href);
        }
    }
    
    @Test
    @SneakyThrows
    void processDeviceDataUpdates_resourceDoesNotExist_doNotTriggerIdsUpdates() {
        // Arrange
        when(offeredResourceApi.getAll()).thenReturn(resourceMultiOutput);
        when(resourceMultiOutput.getEmbedded()).thenReturn(resourceEmbedded);
        when(resourceEmbedded.getEntries()).thenReturn(List.of(resource));
        when(resource.getKeywords()).thenReturn(new ArrayList<>());
    
        // Act
        messageListener.processDeviceDataUpdates(List.of(getMessage()), channel);
    
        // Assert
        verify(messagesApi, never()).notify(any());
    }
    
    @Test
    @SneakyThrows
    void processDeviceDataUpdates_exceptionInDscCommunication_doNotTriggerIdsUpdates() {
        // Arrange
        when(offeredResourceApi.getAll()).thenThrow(ApiInteractionUnsuccessfulException.class);
        
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());
            
            // Act
            messageListener.processDeviceDataUpdates(List.of(getMessage()), channel);
            
            // Assert
            verify(messagesApi, never()).notify(any());
        }
    }
    
    @SneakyThrows
    private Message getMessage() {
        final var properties = new MessageProperties();
        properties.setReceivedRoutingKey("sensor.data.message");
        
        var content = new SensorDataMessage("id", "originId", source, tenant,
                Instant.now(), null, new ArrayList<>());
        var contentJson = objectMapper.writeValueAsString(content);
        
        return new Message(contentJson.getBytes(StandardCharsets.UTF_8), properties);
    }

}
