/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.subscription;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.CatalogApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.CatalogOfferedResourcesApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EntityApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.OfferedResourceRepresentationsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.RepresentationArtifactsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ResourceApi;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.BrokerWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceRemovalWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.WorkflowApi;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.CatalogInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RepresentationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ArtifactEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.CatalogEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.OfferedResourceEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.RepresentationEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.CatalogLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.multi.CatalogMultiOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.CatalogOfferedResourceRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.OfferedResourceRepresentationRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.RepresentationArtifactRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.CatalogOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.RepresentationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.siliconeconomy.iotbroker.idswrapper.config.MappingConfiguration;
import org.siliconeconomy.iotbroker.idswrapper.service.DeviceFilter;
import org.siliconeconomy.iotbroker.idswrapper.service.DeviceMappingService;
import org.siliconeconomy.iotbroker.idswrapper.service.ResourceCreationService;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Test for DeviceManagementMessageListener.
 *
 * @author Ronja Quensel
 */
@SpringBootTest(classes = {DeviceManagementMessageListener.class, MappingConfiguration.class})
class DeviceManagementMessageListenerTest {

    final String EXAMPLE_URI = "http://localhost:8080/api/example/06dc1644-f58d-4562-836f" +
            "-197c0a65c0d6";
    
    @Value("#{'${ids.metadata-broker.urls}'.split(';')}")
    private List<URI> brokers;

    @Autowired
    private DeviceManagementMessageListener messageListener;

    @Autowired
    private @Qualifier("iotBrokerWrapperObjectMapper") ObjectMapper objectMapper;

    @MockBean
    private DeviceMappingService mappingService;

    @MockBean
    private ResourceCreationService creationService;

    @MockBean
    private DscOperator dscOperator;

    @MockBean
    private WorkflowApi workflowApi;

    @MockBean
    private ResourceRemovalWorkflow removalWorkflow;

    @MockBean
    private EntityApi entityApi;

    @MockBean
    private CatalogApi catalogApi;
    
    @MockBean
    private DeviceFilter deviceFilter;

    @Mock
    private Channel channel;

    @Mock
    private CatalogMultiOutput catalogMultiOutput;

    @Mock
    private CatalogEmbedded catalogEmbedded;

    @Mock
    private CatalogOutput catalogOutput;

    @Mock
    private CatalogLinks catalogLinks;

    @MockBean
    private CatalogOfferedResourcesApi catalogResourcesApi;

    @Mock
    private CatalogOfferedResourceRelationOutput catalogResourcesOutput;

    @Mock
    private OfferedResourceEmbedded resourceEmbedded;

    @Mock
    private OfferedResourceOutput resourceOutput;
    
    @Mock
    private OfferedResourceLinks resourceLinks;

    @MockBean
    private ResourceApi resourceApi;

    @MockBean
    private OfferedResourceApi offeredResourceApi;

    @MockBean
    private OfferedResourceRepresentationsApi resourceRepresentationsApi;

    @Mock
    private OfferedResourceRepresentationRelationOutput resourceRepresentationsOutput;

    @Mock
    private RepresentationEmbedded representationEmbedded;

    @Mock
    private RepresentationOutput representationOutput;

    @MockBean
    private RepresentationApi representationApi;

    @MockBean
    private RepresentationArtifactsApi representationArtifactsApi;

    @Mock
    private RepresentationArtifactRelationOutput representationArtifactsOutput;

    @Mock
    private ArtifactEmbedded artifactEmbedded;

    @Mock
    private ArtifactOutput artifactOutput;

    @MockBean
    private ArtifactApi artifactApi;

    @MockBean
    private BrokerWorkflow brokerWorkflow;

    @Mock
    private Link catalogLink;
    
    @Mock
    private Link resourceLink;

    private final String typeIdentifier = "identifier";

    private final String instanceId = "instanceId";

    @BeforeEach
    @SneakyThrows
    void init() {
        when(dscOperator.entities()).thenReturn(entityApi);
        when(entityApi.catalogs()).thenReturn(catalogApi);

        when(dscOperator.workflows()).thenReturn(workflowApi);
        when(workflowApi.resourceRemoval()).thenReturn(removalWorkflow);

        when(workflowApi.broker()).thenReturn(brokerWorkflow);
        
        when(deviceFilter.isProcessingAllowed(any(DeviceTypeUpdate.class))).thenReturn(true);
        when(deviceFilter.isProcessingAllowed(any(DeviceInstanceUpdate.class))).thenReturn(true);
    
        when(catalogApi.getAll()).thenReturn(catalogMultiOutput);
        when(catalogMultiOutput.getEmbedded()).thenReturn(catalogEmbedded);
        when(catalogEmbedded.getEntries()).thenReturn(List.of(catalogOutput));
        when(catalogOutput.getTitle()).thenReturn(typeIdentifier);
    }
    
    @Test
    void constructor_supplyNullDscOperator_throwException() {
        assertThatThrownBy(() -> new DeviceManagementMessageListener(null, mappingService, creationService, objectMapper, deviceFilter))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void constructor_supplyNullDeviceMappingService_throwException() {
        assertThatThrownBy(() -> new DeviceManagementMessageListener(dscOperator, null, creationService, objectMapper, deviceFilter))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void constructor_supplyNullResourceCreationService_throwException() {
        assertThatThrownBy(() -> new DeviceManagementMessageListener(dscOperator, mappingService, null, objectMapper, deviceFilter))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void constructor_supplyNullObjectMapper_throwException() {
        assertThatThrownBy(() -> new DeviceManagementMessageListener(dscOperator, mappingService, creationService, null, deviceFilter))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void constructor_supplyNullDeviceFilter_throwException() {
        assertThatThrownBy(() -> new DeviceManagementMessageListener(dscOperator, mappingService, creationService, objectMapper, null))
                .isInstanceOf(NullPointerException.class);
    }
    
    @Test
    void processDeviceTypeUpdate_notAllowedByWhitelist_doNothing() {
        // Arrange
        var deviceType = getDeviceType();
        var message = getDeviceTypeMessage(deviceType, DeviceTypeUpdate.Type.CREATED);
    
        when(deviceFilter.isProcessingAllowed(any(DeviceTypeUpdate.class))).thenReturn(false);
    
        // Act
        messageListener.processDeviceTypeUpdate(List.of(message), channel);
        
        // Assert
        verifyNoInteractions(dscOperator);
    }

    @Test
    @SneakyThrows
    void processDeviceTypeUpdate_created_createNewCatalog() {
        // Arrange
        var deviceType = getDeviceType();
        var message = getDeviceTypeMessage(deviceType, DeviceTypeUpdate.Type.CREATED);

        var catalogInput = new CatalogInput();
        when(mappingService.deviceTypeToCatalog(any())).thenReturn(catalogInput);
        when(catalogApi.create(any())).thenReturn(new CatalogOutput());

        // Act
        messageListener.processDeviceTypeUpdate(List.of(message), channel);

        // Assert
        verify(catalogApi, only()).create(catalogInput);
        
        verify(brokerWorkflow).registerAllResources(brokers.get(0));
        verify(brokerWorkflow).registerAllResources(brokers.get(1));
        verifyNoMoreInteractions(brokerWorkflow);
    }
    
    @Test
    @SneakyThrows
    void processDeviceTypeUpdate_created_noBrokersDefined_createNewCatalog() {
        // Arrange
        emptyBrokerList();
        
        var deviceType = getDeviceType();
        var message = getDeviceTypeMessage(deviceType, DeviceTypeUpdate.Type.CREATED);
        
        var catalogInput = new CatalogInput();
        when(mappingService.deviceTypeToCatalog(any())).thenReturn(catalogInput);
        when(catalogApi.create(any())).thenReturn(new CatalogOutput());
        
        // Act
        messageListener.processDeviceTypeUpdate(List.of(message), channel);
        
        // Assert
        verify(catalogApi, only()).create(catalogInput);
        verifyNoInteractions(brokerWorkflow);
        
        // Clean-up
        restoreBrokerList();
    }
    
    @Test
    @SneakyThrows
    void processDeviceTypeUpdate_created_errorInBrokerCommunication_createNewCatalog() {
        // Arrange
        var deviceType = getDeviceType();
        var message = getDeviceTypeMessage(deviceType, DeviceTypeUpdate.Type.CREATED);
        
        var catalogInput = new CatalogInput();
        when(mappingService.deviceTypeToCatalog(any())).thenReturn(catalogInput);
        when(catalogApi.create(any())).thenReturn(new CatalogOutput());
        doThrow(ApiInteractionUnsuccessfulException.class).when(brokerWorkflow).registerAllResources(any());
        
        // Act
        messageListener.processDeviceTypeUpdate(List.of(message), channel);
        
        // Assert
        verify(catalogApi, only()).create(catalogInput);
    
        verify(brokerWorkflow).registerAllResources(brokers.get(0));
        verify(brokerWorkflow).registerAllResources(brokers.get(1));
        verifyNoMoreInteractions(brokerWorkflow);
    }

    @Test
    @SneakyThrows
    void processDeviceTypeUpdate_modified_updateExistingCatalog() {
        // Arrange
        final var deviceType = getDeviceType();
        final var message = getDeviceTypeMessage(deviceType, DeviceTypeUpdate.Type.MODIFIED);

        final var catalogInput = new CatalogInput();
        when(mappingService.deviceTypeToCatalog(any())).thenReturn(catalogInput);

        doNothing().when(catalogApi).update(any(), any());

        final var catalogId = UUID.randomUUID();

        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(catalogId);

            // Act
            messageListener.processDeviceTypeUpdate(List.of(message), channel);

            // Assert
            verify(catalogApi, times(1)).getAll();
            verify(catalogApi, times(1)).update(catalogId, catalogInput);
            verifyNoMoreInteractions(catalogApi);
        }
    
        verify(brokerWorkflow).registerAllResources(brokers.get(0));
        verify(brokerWorkflow).registerAllResources(brokers.get(1));
        verifyNoMoreInteractions(brokerWorkflow);
    }

    @Test
    @SneakyThrows
    void processDeviceTypeUpdate_modified_catalogDoesNotExist_doNotUpdate() {
        // Arrange
        final var deviceType = getDeviceType();
        final var message = getDeviceTypeMessage(deviceType, DeviceTypeUpdate.Type.MODIFIED);
        
        when(catalogEmbedded.getEntries()).thenReturn(new ArrayList<>());

        // Act

        messageListener.processDeviceTypeUpdate(List.of(message), channel);
        
        // Assert
        verify(catalogApi, only()).getAll();
        verifyNoInteractions(brokerWorkflow);
    }
    
    @Test
    @SneakyThrows
    void processDeviceTypeUpdate_deleted_deleteExistingCatalog() {
        // Arrange
        final var deviceType = getDeviceType();
        final var message = getDeviceTypeMessage(deviceType, DeviceTypeUpdate.Type.DELETED);
        
        doNothing().when(removalWorkflow).deleteCatalog(any(CatalogOutput.class));

        // Act
        messageListener.processDeviceTypeUpdate(List.of(message), channel);

        // Assert
        verify(catalogApi, only()).getAll();
        verify(removalWorkflow, only()).deleteCatalog(catalogOutput);
    
        verify(brokerWorkflow).registerAllResources(brokers.get(0));
        verify(brokerWorkflow).registerAllResources(brokers.get(1));
        verifyNoMoreInteractions(brokerWorkflow);
    }
    
    @Test
    @SneakyThrows
    void processDeviceTypeUpdate_deleted_catalogDoesNotExist_doNothing() {
        // Arrange
        final var deviceType = getDeviceType();
        final var message = getDeviceTypeMessage(deviceType, DeviceTypeUpdate.Type.DELETED);
        
        when(catalogEmbedded.getEntries()).thenReturn(new ArrayList<>());
        
        // Act
        messageListener.processDeviceTypeUpdate(List.of(message), channel);
        
        // Assert
        verify(catalogApi, only()).getAll();
        verifyNoInteractions(removalWorkflow);
        verifyNoInteractions(brokerWorkflow);
    }
    
    @Test
    void processDeviceInstanceUpdate_notAllowedByWhitelist_doNothing() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.CREATED);
    
        when(deviceFilter.isProcessingAllowed(any(DeviceInstanceUpdate.class))).thenReturn(false);
    
        // Act
        messageListener.processDeviceTypeUpdate(List.of(message), channel);
    
        // Assert
        verifyNoInteractions(dscOperator);
    }
    
    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_created_createResource() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.CREATED);
        final var catalogUri = URI.create("https://catalog");

        when(catalogOutput.getLinks()).thenReturn(catalogLinks);
        when(catalogLinks.getSelf()).thenReturn(catalogLink);
        when(catalogLink.getHref()).thenReturn(catalogUri.toString());
        
        when(creationService.createResource(any(), any())).thenReturn(resourceOutput);
        when(resourceOutput.getLinks()).thenReturn(resourceLinks);
        when(resourceLinks.getSelf()).thenReturn(resourceLink);
        when(resourceLink.getHref()).thenReturn(EXAMPLE_URI);
        
        // Act
        messageListener.processDeviceInstanceUpdate(List.of(message), channel);
        
        // Assert
        verify(catalogApi, only()).getAll();
        verify(creationService, only()).createResource(catalogUri, deviceInstance);

        ArgumentMatcher<List<URI>> resourceUriMatcher = uris -> uris.size() == 1 && uris.get(0).toString().equals(EXAMPLE_URI);
        verify(brokerWorkflow).registerResources(eq(brokers.get(0)), argThat(resourceUriMatcher));
        verify(brokerWorkflow).registerResources(eq(brokers.get(1)), argThat(resourceUriMatcher));
        verifyNoMoreInteractions(brokerWorkflow);
    }
    
    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_created_noBrokersDefined_createResource() {
        // Arrange
        emptyBrokerList();
        
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.CREATED);
        final var catalogUri = URI.create("https://catalog");

        when(catalogOutput.getLinks()).thenReturn(catalogLinks);
        when(catalogLinks.getSelf()).thenReturn(catalogLink);
        when(catalogLink.getHref()).thenReturn(catalogUri.toString());
        
        when(creationService.createResource(any(), any())).thenReturn(resourceOutput);
        
        // Act
        messageListener.processDeviceInstanceUpdate(List.of(message), channel);
        
        // Assert
        verify(catalogApi, only()).getAll();
        verify(creationService, only()).createResource(catalogUri, deviceInstance);
    
        verifyNoInteractions(brokerWorkflow);
    
        // Clean-up
        restoreBrokerList();
    }
    
    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_created_errorInBrokerCommunication_createResource() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.CREATED);
        final var catalogUri = URI.create("https://catalog");

        when(catalogOutput.getLinks()).thenReturn(catalogLinks);
        when(catalogLinks.getSelf()).thenReturn(catalogLink);
        when(catalogLink.getHref()).thenReturn(catalogUri.toString());
        
        when(creationService.createResource(any(), any())).thenReturn(resourceOutput);
        when(resourceOutput.getLinks()).thenReturn(resourceLinks);
        when(resourceLinks.getSelf()).thenReturn(resourceLink);
        when(resourceLink.getHref()).thenReturn(EXAMPLE_URI);
        
        doThrow(ApiInteractionUnsuccessfulException.class).when(brokerWorkflow).registerAllResources(any());
        
        // Act
        messageListener.processDeviceInstanceUpdate(List.of(message), channel);
        
        // Assert
        verify(catalogApi, only()).getAll();
        verify(creationService, only()).createResource(catalogUri, deviceInstance);
    
        ArgumentMatcher<List<URI>> resourceUriMatcher = uris -> uris.size() == 1 && uris.get(0).toString().equals(EXAMPLE_URI);
        verify(brokerWorkflow).registerResources(eq(brokers.get(0)), argThat(resourceUriMatcher));
        verify(brokerWorkflow).registerResources(eq(brokers.get(1)), argThat(resourceUriMatcher));
        verifyNoMoreInteractions(brokerWorkflow);
    }
    
    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_created_catalogDoesNotExist_doNotCreate() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.CREATED);
        
        when(catalogEmbedded.getEntries()).thenReturn(new ArrayList<>());
    
        // Act
        messageListener.processDeviceInstanceUpdate(List.of(message), channel);
        
        // Assert
        verifyNoInteractions(creationService);
    }
    
    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_modified_updateExistingResource() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.MODIFIED);
        final var resourceId = UUID.randomUUID();
        final var representationId = UUID.randomUUID();
        final var artifactId = UUID.randomUUID();
        
        when(catalogApi.offers()).thenReturn(catalogResourcesApi);
        when(catalogResourcesApi.getAll(any())).thenReturn(catalogResourcesOutput);
        when(catalogResourcesOutput.getEmbedded()).thenReturn(resourceEmbedded);
        when(resourceEmbedded.getEntries()).thenReturn(List.of(resourceOutput));
        when(resourceOutput.getTitle()).thenReturn(instanceId);
        when(resourceOutput.getLinks()).thenReturn(resourceLinks);
        when(resourceLinks.getSelf()).thenReturn(resourceLink);
        when(resourceLink.getHref()).thenReturn(EXAMPLE_URI);
        
        when(entityApi.resources()).thenReturn(resourceApi);
        when(resourceApi.offers()).thenReturn(offeredResourceApi);
        when(offeredResourceApi.representations()).thenReturn(resourceRepresentationsApi);
        when(resourceRepresentationsApi.getAll(any())).thenReturn(resourceRepresentationsOutput);
        when(resourceRepresentationsOutput.getEmbedded()).thenReturn(representationEmbedded);
        when(representationEmbedded.getEntries()).thenReturn(List.of(representationOutput));
        
        when(entityApi.representations()).thenReturn(representationApi);
        when(representationApi.artifacts()).thenReturn(representationArtifactsApi);
        when(representationArtifactsApi.getAll(any())).thenReturn(representationArtifactsOutput);
        when(representationArtifactsOutput.getEmbedded()).thenReturn(artifactEmbedded);
        when(artifactEmbedded.getEntries()).thenReturn(List.of(artifactOutput));
    
        var resourceInput = new OfferedResourceInput();
        when(mappingService.deviceInstanceToResource(any())).thenReturn(resourceInput);
        doNothing().when(offeredResourceApi).update(any(), any());
    
        var representationInput = new RepresentationInput();
        when(mappingService.deviceInstanceToRepresentation(any())).thenReturn(representationInput);
        doNothing().when(representationApi).update(any(), any());
        
        var artifactInput = new ArtifactInput();
        when(mappingService.deviceInstanceToArtifact(any())).thenReturn(artifactInput);
        when(entityApi.artifacts()).thenReturn(artifactApi);
        doNothing().when(artifactApi).update(any(), any());

        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any(OfferedResourceOutput.class)))
                    .thenReturn(resourceId);
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any(RepresentationOutput.class)))
                    .thenReturn(representationId);
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any(ArtifactOutput.class)))
                    .thenReturn(artifactId);
    
            // Act
            messageListener.processDeviceInstanceUpdate(List.of(message), channel);
    
            // Assert
            verify(catalogApi, times(1)).getAll();
            verify(catalogApi, times(1)).offers();
            verifyNoMoreInteractions(catalogApi);
            
            verify(mappingService, times(1)).deviceInstanceToResource(deviceInstance);
            verify(mappingService, times(1)).deviceInstanceToRepresentation(deviceInstance);
            verify(mappingService, times(1)).deviceInstanceToArtifact(deviceInstance);
            verifyNoMoreInteractions(mappingService);
            
            verify(offeredResourceApi, times(1)).update(resourceId, resourceInput);
            verify(offeredResourceApi, times(1)).representations();
            verifyNoMoreInteractions(offeredResourceApi);
            
            verify(representationApi, times(1)).update(representationId, representationInput);
            verify(representationApi, times(1)).artifacts();
            verifyNoMoreInteractions(offeredResourceApi);

            verify(artifactApi, times(1)).update(artifactId, artifactInput);
            verifyNoMoreInteractions(offeredResourceApi);
        }
    
        ArgumentMatcher<List<URI>> resourceUriMatcher = uris -> uris.size() == 1 && uris.get(0).toString().equals(EXAMPLE_URI);
        verify(brokerWorkflow).registerResources(eq(brokers.get(0)), argThat(resourceUriMatcher));
        verify(brokerWorkflow).registerResources(eq(brokers.get(1)), argThat(resourceUriMatcher));
        verifyNoMoreInteractions(brokerWorkflow);
    }
    
    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_modified_catalogDoesNotExist_doNotUpdate() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.MODIFIED);

        when(catalogOutput.getTitle()).thenReturn("not the correct title");
        
        // Act
        messageListener.processDeviceInstanceUpdate(List.of(message), channel);
        
        // Assert
        verify(offeredResourceApi, never()).update(any(), any());
        verify(representationApi, never()).update(any(), any());
        verify(artifactApi, never()).update(any(), any());
        verifyNoInteractions(brokerWorkflow);
    }
    
    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_modified_resourceDoesNotExist_doNotUpdate() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.MODIFIED);
    
        when(catalogApi.offers()).thenReturn(catalogResourcesApi);
        when(catalogResourcesApi.getAll(any())).thenReturn(catalogResourcesOutput);
        when(catalogResourcesOutput.getEmbedded()).thenReturn(resourceEmbedded);
        when(resourceEmbedded.getEntries()).thenReturn(List.of(resourceOutput));
        when(resourceOutput.getTitle()).thenReturn("not the correct title");
    
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());

            // Act
            messageListener.processDeviceInstanceUpdate(List.of(message), channel);
    
            // Assert
            verify(offeredResourceApi, never()).update(any(), any());
            verify(representationApi, never()).update(any(), any());
            verify(artifactApi, never()).update(any(), any());
        }
    
        verifyNoInteractions(brokerWorkflow);
    }
    
    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_modified_representationDoesNotExist_doNotUpdate() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.MODIFIED);
        
        when(catalogApi.offers()).thenReturn(catalogResourcesApi);
        when(catalogResourcesApi.getAll(any())).thenReturn(catalogResourcesOutput);
        when(catalogResourcesOutput.getEmbedded()).thenReturn(resourceEmbedded);
        when(resourceEmbedded.getEntries()).thenReturn(List.of(resourceOutput));
        when(resourceOutput.getTitle()).thenReturn(instanceId);
    
        when(entityApi.resources()).thenReturn(resourceApi);
        when(resourceApi.offers()).thenReturn(offeredResourceApi);
        doNothing().when(offeredResourceApi).update(any(), any());
        when(offeredResourceApi.representations()).thenReturn(resourceRepresentationsApi);
        when(resourceRepresentationsApi.getAll(any())).thenReturn(resourceRepresentationsOutput);
        when(resourceRepresentationsOutput.getEmbedded()).thenReturn(representationEmbedded);
        when(representationEmbedded.getEntries()).thenReturn(new ArrayList<>());
    
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());
    
            // Act
            messageListener.processDeviceInstanceUpdate(List.of(message), channel);
    
            // Assert
            verify(offeredResourceApi, never()).update(any(), any());
            verify(representationApi, never()).update(any(), any());
            verify(artifactApi, never()).update(any(), any());
        }
    
        verifyNoInteractions(brokerWorkflow);
    }
    
    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_modified_artifactDoesNotExist_doNotUpdate() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.MODIFIED);

        when(catalogApi.offers()).thenReturn(catalogResourcesApi);
        when(catalogResourcesApi.getAll(any())).thenReturn(catalogResourcesOutput);
        when(catalogResourcesOutput.getEmbedded()).thenReturn(resourceEmbedded);
        when(resourceEmbedded.getEntries()).thenReturn(List.of(resourceOutput));
        when(resourceOutput.getTitle()).thenReturn(instanceId);
        
        when(entityApi.resources()).thenReturn(resourceApi);
        when(resourceApi.offers()).thenReturn(offeredResourceApi);
        when(offeredResourceApi.representations()).thenReturn(resourceRepresentationsApi);
        when(resourceRepresentationsApi.getAll(any())).thenReturn(resourceRepresentationsOutput);
        when(resourceRepresentationsOutput.getEmbedded()).thenReturn(representationEmbedded);
        when(representationEmbedded.getEntries()).thenReturn(List.of(representationOutput));
    
        when(entityApi.representations()).thenReturn(representationApi);
        when(representationApi.artifacts()).thenReturn(representationArtifactsApi);
        when(representationArtifactsApi.getAll(any())).thenReturn(representationArtifactsOutput);
        when(representationArtifactsOutput.getEmbedded()).thenReturn(artifactEmbedded);
        when(artifactEmbedded.getEntries()).thenReturn(new ArrayList<>());
    
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());
    
            // Act
            messageListener.processDeviceInstanceUpdate(List.of(message), channel);
    
            // Assert
            verify(offeredResourceApi, never()).update(any(), any());
            verify(representationApi, never()).update(any(), any());
            verify(artifactApi, never()).update(any(), any());
        }
    
        verifyNoInteractions(brokerWorkflow);
    }

    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_deleted_deleteExistingResource() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.DELETED);

        when(catalogApi.offers()).thenReturn(catalogResourcesApi);
        when(catalogResourcesApi.getAll(any())).thenReturn(catalogResourcesOutput);
        when(catalogResourcesOutput.getEmbedded()).thenReturn(resourceEmbedded);
        when(resourceEmbedded.getEntries()).thenReturn(List.of(resourceOutput));
        when(resourceOutput.getTitle()).thenReturn(instanceId);
        when(resourceOutput.getLinks()).thenReturn(resourceLinks);
        when(resourceLinks.getSelf()).thenReturn(resourceLink);
        when(resourceLink.getHref()).thenReturn(EXAMPLE_URI);
        
        doNothing().when(removalWorkflow).deleteFullResource(any(OfferedResourceOutput.class));
    
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());
    
            // Act
            messageListener.processDeviceInstanceUpdate(List.of(message), channel);
    
            // Assert
            verify(catalogApi, times(1)).getAll();
            verify(catalogApi, times(1)).offers();
            verifyNoMoreInteractions(catalogApi);
            verify(removalWorkflow, only()).deleteFullResource(resourceOutput);
        }
    
        ArgumentMatcher<List<URI>> resourceUriMatcher = uris -> uris.size() == 1 && uris.get(0).toString().equals(EXAMPLE_URI);
        verify(brokerWorkflow).unregisterResources(eq(brokers.get(0)), argThat(resourceUriMatcher));
        verify(brokerWorkflow).unregisterResources(eq(brokers.get(1)), argThat(resourceUriMatcher));
        verifyNoMoreInteractions(brokerWorkflow);
    }

    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_deleted_noBrokersDefined_deleteExistingResource() {
        // Arrange
        emptyBrokerList();
        
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.DELETED);
        
        when(catalogApi.offers()).thenReturn(catalogResourcesApi);
        when(catalogResourcesApi.getAll(any())).thenReturn(catalogResourcesOutput);
        when(catalogResourcesOutput.getEmbedded()).thenReturn(resourceEmbedded);
        when(resourceEmbedded.getEntries()).thenReturn(List.of(resourceOutput));
        when(resourceOutput.getTitle()).thenReturn(instanceId);
        when(resourceOutput.getLinks()).thenReturn(resourceLinks);
        when(resourceLinks.getSelf()).thenReturn(resourceLink);
        when(resourceLink.getHref()).thenReturn(EXAMPLE_URI);
        
        doNothing().when(removalWorkflow).deleteFullResource(any(OfferedResourceOutput.class));
        
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());
            
            // Act
            messageListener.processDeviceInstanceUpdate(List.of(message), channel);
            
            // Assert
            verify(catalogApi, times(1)).getAll();
            verify(catalogApi, times(1)).offers();
            verifyNoMoreInteractions(catalogApi);
            verify(removalWorkflow, only()).deleteFullResource(resourceOutput);
        }
    
        verifyNoInteractions(brokerWorkflow);
    
        // Clean-up
        restoreBrokerList();
    }
    
    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_deleted_errorInBrokerCommunication_deleteExistingResource() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.DELETED);
        
        when(catalogApi.offers()).thenReturn(catalogResourcesApi);
        when(catalogResourcesApi.getAll(any())).thenReturn(catalogResourcesOutput);
        when(catalogResourcesOutput.getEmbedded()).thenReturn(resourceEmbedded);
        when(resourceEmbedded.getEntries()).thenReturn(List.of(resourceOutput));
        when(resourceOutput.getTitle()).thenReturn(instanceId);
        when(resourceOutput.getLinks()).thenReturn(resourceLinks);
        when(resourceLinks.getSelf()).thenReturn(resourceLink);
        when(resourceLink.getHref()).thenReturn(EXAMPLE_URI);
        
        doNothing().when(removalWorkflow).deleteFullResource(any(OfferedResourceOutput.class));
        doThrow(ApiInteractionUnsuccessfulException.class).when(brokerWorkflow).registerAllResources(any());
        
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());
            
            // Act
            messageListener.processDeviceInstanceUpdate(List.of(message), channel);
            
            // Assert
            verify(catalogApi, times(1)).getAll();
            verify(catalogApi, times(1)).offers();
            verifyNoMoreInteractions(catalogApi);
            verify(removalWorkflow, only()).deleteFullResource(resourceOutput);
        }
        
        ArgumentMatcher<List<URI>> resourceUriMatcher = uris -> uris.size() == 1 && uris.get(0).toString().equals(EXAMPLE_URI);
        verify(brokerWorkflow).unregisterResources(eq(brokers.get(0)), argThat(resourceUriMatcher));
        verify(brokerWorkflow).unregisterResources(eq(brokers.get(1)), argThat(resourceUriMatcher));
        verifyNoMoreInteractions(brokerWorkflow);
    }
    
    @Test
    @SneakyThrows
    void processDeviceInstanceUpdate_deleted_resourceDoesNotExist_doNothing() {
        // Arrange
        final var deviceInstance = getDeviceInstance();
        final var message = getDeviceInstanceMessage(deviceInstance,
                DeviceInstanceUpdate.Type.DELETED);

        when(catalogApi.offers()).thenReturn(catalogResourcesApi);
        when(catalogResourcesApi.getAll(any())).thenReturn(catalogResourcesOutput);
        when(catalogResourcesOutput.getEmbedded()).thenReturn(resourceEmbedded);
        when(resourceEmbedded.getEntries()).thenReturn(new ArrayList<>());
        
        try (MockedStatic<UuidUtils> uuidUtils = mockStatic(UuidUtils.class)) {
            uuidUtils.when(() -> UuidUtils.getUuidFromOutput(any())).thenReturn(UUID.randomUUID());
            
            // Act
            messageListener.processDeviceInstanceUpdate(List.of(message), channel);
            
            // Assert
            verify(catalogApi, times(1)).getAll();
            verify(catalogApi, times(1)).offers();
            verifyNoMoreInteractions(catalogApi);
            verifyNoInteractions(removalWorkflow);
        }
    
        verifyNoInteractions(brokerWorkflow);
    }
    
    private DeviceType getDeviceType() {
        return new DeviceType("id", "source", typeIdentifier, new HashSet<>(),
                "description", true, true, true);
    }
    
    @SneakyThrows
    private Message getDeviceTypeMessage(final DeviceType deviceType,
                                         final DeviceTypeUpdate.Type updateType) {
        final var properties = new MessageProperties();
        properties.setReceivedRoutingKey(format("deviceType.%s.update",
                deviceType.getIdentifier()));
        
        final var update = new DeviceTypeUpdate(deviceType, deviceType, updateType);
        final var updateJson = objectMapper.writeValueAsString(update);
        
        return new Message(updateJson.getBytes(StandardCharsets.UTF_8), properties);
    }
    
    private DeviceInstance getDeviceInstance() {
        return new DeviceInstance(instanceId, "source", "tenant", typeIdentifier, Instant.now(),
                Instant.now(), true, "desc", "1.0", "1.0");
    }
    
    @SneakyThrows
    private Message getDeviceInstanceMessage(final DeviceInstance deviceInstance,
                                             final DeviceInstanceUpdate.Type updateType) {
        final var properties = new MessageProperties();
        properties.setReceivedRoutingKey(format("deviceInstance.%s.update",
                typeIdentifier));
        
        final var update = new DeviceInstanceUpdate(deviceInstance, deviceInstance, updateType);
        final var updateJson = objectMapper.writeValueAsString(update);
    
        return new Message(updateJson.getBytes(StandardCharsets.UTF_8), properties);
    }
    
    private void emptyBrokerList() {
        ReflectionTestUtils.setField(messageListener, "brokers", new ArrayList<>());
    }
    
    private void restoreBrokerList() {
        ReflectionTestUtils.setField(messageListener, "brokers", brokers);
    }
    
}
