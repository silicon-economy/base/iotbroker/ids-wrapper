/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.subscription.deserialization;

import java.time.Instant;
import java.util.HashSet;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for Jackson MixIn classes.
 *
 * @author Ronja Quensel
 */
class MixInTest {
    
    @Test
    void createDeviceTypeMixIn() {
        // Act
        final var result = new DeviceTypeMixIn("id", "source", "id", new HashSet<>(),
                "desc", true, true, true);
        
        // Assert
        assertThat(result).isNotNull();
    }
    
    @Test
    void createDeviceTypeUpdateMixIn() {
        // Arrange
        final var type = new DeviceType("id", "source", "id", new HashSet<>(),
                "desc", true, true, true);
        
        // Act
        final var result = new DeviceTypeUpdateMixIn(type, type, DeviceTypeUpdate.Type.CREATED);
        
        // Assert
        assertThat(result).isNotNull();
    }
    
    @Test
    void createDeviceInstanceMixIn() {
        // Act
        final var result = new DeviceInstanceMixIn("id", "source", "tenant", "typeId",
                Instant.now(), Instant.now(), true, "desc", "1", "1");
        
        // Assert
        assertThat(result).isNotNull();
    }
    
    @Test
    void createDeviceInstanceUpdateMixIn() {
        // Arrange
        final var instance = new DeviceInstance("id", "source", "tenant", "typeId",
                Instant.now(), Instant.now(), true, "desc", "1", "1");
        
        // Act
        final var result = new DeviceInstanceUpdateMixIn(instance, instance, DeviceInstanceUpdate.Type.CREATED);
    
        // Assert
        assertThat(result).isNotNull();
    }
    
}
