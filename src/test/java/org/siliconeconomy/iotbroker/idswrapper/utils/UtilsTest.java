/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.iotbroker.idswrapper.utils;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

/**
 * Test for Utils
 *
 * @author Ronja Quensel
 */
class UtilsTest {
    
    @Test
    void identifierFromRoutingKey_validRoutingKey_returnIdentifier() {
        // Arrange
        var identifier = "identifier";
        var routingKey = format("deviceType.%s.update", identifier);
        
        // Act
        var result = Utils.identifierFromRoutingKey(routingKey);
        
        // Assert
        assertThat(result).isEqualTo(identifier);
    }
    
    @Test
    void identifierFromRoutingKey_invalidRoutingKey_throwException() {
        // Arrange
        var routingKey = "invalid format";
        
        // Act & Assert
        assertThatThrownBy(() -> Utils.identifierFromRoutingKey(routingKey))
                .isInstanceOf(IllegalArgumentException.class);
    }
    
}
